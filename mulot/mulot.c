#if 0

proc build { {selection {}} } {
	file mkdir .build
	if 0 {
		set type KEYBOARD
	} {
		set type MOUSE
	}
	cd .build
	exec sdcc -mmcs51 ../mulotDscr.c -DTYPE_$type -c
	exec sdcc -mmcs51 ../mulot.c     -DTYPE_$type -c
	exec sdcc mulot.rel mulotDscr.rel
	cd ..
}

proc loadAndRun { } {
	ez::reset 1
	ez::loadIhex .build/mulot.ihx
	ez::reset 0
}

proc Debug { } {
	puts stdout [format "0x%08x" [ez::r 0x1ffc] ]
}

proc SetupData { } {
	puts stdout [format "0x%08x" [ez::r 0x1ff8] ]
}

proc go { {selection loadAndRun } } {
	source ../tcl/ez.tcl
	# sudo rmmod go7007_usb go7007
	# echo 'SUBSYSTEM=="usbmisc", ATTR{../../../idVendor}=="04b4", ATTR{../../../idProduct}=="8613", MODE="0600", OWNER="'$USER'"' | sudo tee -a /etc/udev/rules.d/50-ez.rules
	ez::open
	$selection
	ez::close
}

proc clean {} {
	exec rm -rf .build
}

rename clean compile

eval $argv

return

// http://www.danielclemente.com/placa_pi/include-fix/fx2sdly.h

#endif

#include "ez-fx2.h"
#include "ez-fx2sdly.h"
#include "mulotDscr.h"
#include "stdio.h"

__xdata __at(0x1ffc) volatile unsigned char Debug;
__xdata __at(0x1ff8) volatile unsigned char SetupData;

char GotSUD;
char Sleep;
char FullSpeed;

void usbInit();
void usbPoll();
void setupData();

#if 0
void EZUSB_Delay1ms() _naked
{
/*
 Delay for 1 millisecond (1000 microseconds).
 10 cycles * 166.6 ns per cycle is 1.66 microseconds per loop.
 1000 microseconds / 1.66 = 602.  [assumes 24 MHz clock]
*/
/* 12M : */
/*#define LOOP_1ms 301*/
/* 24M : */
/*#define LOOP_1ms 602*/
/* 48M : */
	__asm
#define LOOP_1ms 1204
//	mov a, #0
//	mov dps, a
	mov dptr,#(0xffff - LOOP_1ms)
	mov r4,#5
loop:
	inc dptr  ; 3 cycles
	mov a,dpl ; 2 cycles
	orl a,dph ; 2 cycles
	jnz loop  ; 3 cycles
	ret
	__endasm;
}
#else
void EZUSB_Delay1ms()
{
	int dcnt=1024;
	while(dcnt--)
		;
}
#endif

void EZUSB_Delay(unsigned short ms)
{
	while(ms--)
		EZUSB_Delay1ms();
}

void EZUSB_Resume(void)
{
//	if( USBCS & MSK(RWAKEUP) )
	{
		USBCS = USBCS SET(SIGRESUME);
		EZUSB_Delay(10);
		USBCS = USBCS CLR(SIGRESUME);
	}
}

void EZUSB_Discon(char renum)
{
//	USBCS = USBCS SET(DISCON);  // added for FX first silicon keeper problem
//	WRITEDELAY();               // allow for register write recovery time
//	USBCS = USBCS CLR(DISCOE);  // Tristate the disconnect pin
//	WRITEDELAY();               // allow for register write recovery time
	USBCS = USBCS SET(DISCON);  // Set this high to disconnect USB
	if(renum)               // If renumerate
		USBCS = USBCS SET(RENUM);     // then set renumerate bit

	EZUSB_Delay(1500);      // Wait 1500 ms  (SHK -- If you don't wait this long, some hubs and hosts will not see the disconnect.)

	USBIRQ = 0xff;          // Clear any pending USB interrupt requests.  They're for our old life.
/*
	IN07IRQ = 0xff;         // clear old activity
	OUT07IRQ = 0xff;
*/
	EXIF = EXIF CLR(USBNT);

	USBCS = USBCS CLR(DISCON);  // reconnect USB
//	WRITEDELAY();               // allow for register write recovery time
//	USBCS = USBCS SET(DISCOE);  // Release Tristated disconnect pin
}

int putchar(int c)
{
	SBUF0=c;
	while(!TI_0)
		;
	TI_0=0;
	return 0;
}

//int init() _naked
int main()
{
	Debug     =0x00;
	SetupData =0xff;

	CPUCS = CPUCS CHG(CLKSPD,CLKSPD_48);

	IFCONFIG = IFCONFIG SET(_3048MHZ);

	SCON0 = 0x50;
	// 230K bauds
	UART230 = UART230 SET(UART230_0);
	PCON  = PCON CLR(SMOD0);

	CKCON=0;

	printf("\033[2J");
	printf(__TIME__ "\r\n");

#if 0
	{
		int i=0;
		while(1)
		{
			printf("%d\r\n",i);
			i++;
			EZUSB_Delay(10);
		}
	}
#else
	GotSUD=0;
	FullSpeed=1;

	usbInit();

	EUSB=1;
	ERESI=1;
	INTSETUP = INTSETUP SET(AV2EN);

	USBIE = USBIE SET(SUDAV) SET(SUTOK) SET(SUSP) SET(URES) SET(HSGRANT);
	EA = 1; // Enable 8051 interrupts
	//EZUSB_Resume();
	EZUSB_Discon(1);

	EP1INCS = EP1INCS CLR(EPSTALL);

	while(1)
	{
		if(GotSUD)
		{
			setupData();
			GotSUD=0;
		}

		usbPoll();

/*
#define YELLOW0 BIT(6)
#define YELLOW1 BIT(2)
#define GREEN1  BIT(0)

#define LED_SWAP(bit) IOE^= MSK(bit);
#define LED_SET(bit)  IOE = IOE SET(bit);
#define LED_CLR(bit)  IOE = IOE CLR(bit);
*/

	}
#endif
}


void usbInit()
{
	EP1OUTCFG = 0xB0;
	EP1INCFG = 0xB0; // valid, interrupt
	SYNCDELAY;                    // see TRM section 15.14
	EP2CFG = 0xA2;
	SYNCDELAY;
	EP4CFG = 0xA0;
	SYNCDELAY;
	EP6CFG = 0xE2;
	SYNCDELAY;
	EP8CFG = 0xE0;
	
	// out endpoints do not come up armed
	
	// since the defaults are double buffered we must write dummy byte counts twice
	SYNCDELAY;
	EP2BCL = 0x80;                // arm EP2OUT by writing byte count w/skip.
	SYNCDELAY;
	EP2BCL = 0x80;
	SYNCDELAY;
	EP4BCL = 0x80;                // arm EP4OUT by writing byte count w/skip.
	SYNCDELAY;
	EP4BCL = 0x80;

	AUTOPTRSETUP = AUTOPTRSETUP SET(APTREN);

	SUDPTRCTL = SUDPTRCTL SET(SDPAUTO);
}

void usbPoll()
{
	if(RI_0)
	{
		printf("poll in\r\n");
		RI_0=0;
		if( !(EP1INCS & MSK(EPBUSY) ) )
		{
			printf("send in\r\n");
			#if defined(TYPE_MOUSE)
			EP1INBUF[0] = SBUF0;
			EP1INBUF[1] = 0;
			EP1INBUF[2] = 1;
			EP1INBC = 3;
			#elif defined(TYPE_KEYBOARD)
#define CTRL  BIT(0)
#define SHIFT BIT(1)
#define ALT   BIT(2)
#define META  BIT(3)
#define MOD_LEFT  FIELD(4,0)
#define MOD_RIGHT FIELD(4,4)

#if 0 // mac keyboard
/*

+---+   +---++---++---++---+    +---++---++---++---+   +---++---++---++---+   +---++---++---+   +---++---++---++---+
|   |   |   ||   ||   ||   |    |   ||   ||   ||   |   |   ||   ||   ||   |   |   ||   ||   |   |   ||   ||   ||   |
+---+   +---++---++---++---+    +---++---++---++---+   +---++---++---++---+   +---++---++---+   +---++---++---++---+
+---++---++---++---++---++---++---++---++---++---++---++---++---++--------+   +---++---++---+   +---++---++---++---+
|   ||   ||   ||   ||   ||   ||   ||   ||   ||   ||   ||   ||   ||        |   |   ||   ||   |   |   ||   ||   ||   |
+---++---++---++---++---++---++---++---++---++---++---++---++---++--------+   +---++---++---+   +---++---++---++---+
+-----++---++---++---++---++---++---++---++---++---++---++---++---++------+   +---++---++---+   +---++---++---++---+
|     ||   ||   ||   ||   ||   ||   ||   ||   ||   ||   ||   ||   ||      |   |   ||   ||   |   |   ||   ||   ||   |
+-----++---++---++---++---++---++---++---++---++---++---++---++---+++     |   +---++---++---+   +---++---++---++---+
+-----+ +---++---++---++---++---++---++---++---++---++---++---++---+|     |                     +---++---++---++---+
|     | |   ||   ||   ||   ||   ||   ||   ||   ||   ||   ||   ||   ||     |                     |   ||   ||   ||   |
+-----+ +---++---++---++---++---++---++---++---++---++---++---++---++-----+                     +---++---++---++---+
+----++---++---++---++---++---++---++---++---++---++---++---++------------+        +---+        +---++---++---++---+
|    ||   ||   ||   ||   ||   ||   ||   ||   ||   ||   ||   ||            |        |   |        |   ||   ||   ||   |
+----++---++---++---++---++---++---++---++---++---++---++---++------------+        +---+        +---++---++---+|   |
+-----++-----++-----++-------------------------------++-----++-----++-----+   +---++---++---+   +--------++---+|   |
|     ||     ||     ||                               ||     ||     ||     |   |   ||   ||   |   |        ||   ||   |
+-----++-----++-----++-------------------------------++-----++-----++-----+   +---++---++---+   +--------++---++---+

french azerty up
+---+   +---++---++---++---+    +---++---++---++---+   +---++---++---++---+   +---++---++---+   +---++---++---++---+
|ESC|   |F1 ||F2 ||F3 ||F4 |    |F5 ||F6 ||F7 ||F8 |   |F9 ||F10||F11||F12|   |F13||F14||F15|   |V- ||V+ ||V0 ||Eje|
+---+   +---++---++---++---+    +---++---++---++---+   +---++---++---++---+   +---++---++---+   +---++---++---++---+
+---++---++---++---++---++---++---++---++---++---++---++---++---++--------+   +---++---++---+   +---++---++---++---+
|#  || 1 || 2 || 3 || 4 || 5 || 6 || 7 || 8 || 9 || 0 || � || _ || Bs     |   |aid||hom||pud|   |Ver|| = || / || * |
+---++---++---++---++---++---++---++---++---++---++---++---++---++--------+   +---++---++---+   +---++---++---++---+
+-----++---++---++---++---++---++---++---++---++---++---++---++---++------+   +---++---++---+   +---++---++---++---+
|Tab  || A || Z || E || R || T || Y || U || I || O || P || � || * || Enter|   |sup||end||pdw|   |   ||   ||   || - |
+-----++---++---++---++---++---++---++---++---++---++---++---++---+++     |   +---++---++---+   +---++---++---++---+
+-----+ +---++---++---++---++---++---++---++---++---++---++---++---+|     |                     +---++---++---++---+
|Caps | | Q || S || D || F || G || H || J || K || L || M || % || � ||     |                     |   ||   ||   || + |
+-----+ +---++---++---++---++---++---++---++---++---++---++---++---++-----+                     +---++---++---++---+
+----++---++---++---++---++---++---++---++---++---++---++---++------------+        +---+        +---++---++---++---+
|Shif|| > || W || X || C || V || B || N || ? || . || / || + ||Shift       |        |up |        |   ||   ||   || E |
+----++---++---++---++---++---++---++---++---++---++---++---++------------+        +---+        +---++---++---+| n |
+-----++-----++-----++-------------------------------++-----++-----++-----+   +---++---++---+   +--------++---+| t |
|Ctrl ||Alt  ||Meta ||        Space                  ||Meta ||Alt  ||Ctrl |   |lef||dow||rig|   |        ||   || e |
+-----++-----++-----++-------------------------------++-----++-----++-----+   +---++---++---+   +--------++---++---+

french azerty down
+---+   +---++---++---++---+    +---++---++---++---+   +---++---++---++---+   +---++---++---+   +---++---++---++---+
|ESC|   |F1 ||F2 ||F3 ||F4 |    |F5 ||F6 ||F7 ||F8 |   |F9 ||F10||F11||F12|   |F13||F14||F15|   |V- ||V+ ||V0 ||Eje|
+---+   +---++---++---++---+    +---++---++---++---+   +---++---++---++---+   +---++---++---+   +---++---++---++---+
+---++---++---++---++---++---++---++---++---++---++---++---++---++--------+   +---++---++---+   +---++---++---++---+
|@  || & || � || " || ' || ( || $ || � || ! || � || � || ) || - || Bs     |   |aid||hom||pud|   |Ver|| = || / || * |
+---++---++---++---++---++---++---++---++---++---++---++---++---++--------+   +---++---++---+   +---++---++---++---+
+-----++---++---++---++---++---++---++---++---++---++---++---++---++------+   +---++---++---+   +---++---++---++---+
|Tab  || a || z || e || r || t || y || u || i || o || p || � || * || Enter|   |sup||end||pdw|   |   ||   ||   || - |
+-----++---++---++---++---++---++---++---++---++---++---++---++---+++     |   +---++---++---+   +---++---++---++---+
+-----+ +---++---++---++---++---++---++---++---++---++---++---++---+|     |                     +---++---++---++---+
|Caps | | q || s || d || f || g || h || j || k || l || m || % || � ||     |                     |   ||   ||   || + |
+-----+ +---++---++---++---++---++---++---++---++---++---++---++---++-----+                     +---++---++---++---+
+----++---++---++---++---++---++---++---++---++---++---++---++------------+        +---+        +---++---++---++---+
|Shif|| < || w || x || c || v || b || n || , || ; || : || = ||Shift       |        |up |        |   ||   ||   || E |
+----++---++---++---++---++---++---++---++---++---++---++---++------------+        +---+        +---++---++---+| n |
+-----++-----++-----++-------------------------------++-----++-----++-----+   +---++---++---+   +--------++---+| t |
|Ctrl ||Alt  ||Meta ||        Space                  ||Meta ||Alt  ||Ctrl |   |lef||dow||rig|   |        ||   || e |
+-----++-----++-----++-------------------------------++-----++-----++-----+   +---++---++---+   +--------++---++---+

usb key codes
+---+   +---++---++---++---+    +---++---++---++---+   +---++---++---++---+   +---++---++---+   +---++---++---++---+
| 29|   | 3a|| 3b|| 3c|| 3d|    | 3e|| 3f|| 40|| 41|   | 42|| 43|| 44|| 45|   | 68|| 69|| 6a|   | *0|| *1|| *2|| *3|
+---+   +---++---++---++---+    +---++---++---++---+   +---++---++---++---+   +---++---++---+   +---++---++---++---+
+---++---++---++---++---++---++---++---++---++---++---++---++---++--------+   +---++---++---+   +---++---++---++---+
| 35|| 1e|| 1f|| 20|| 21|| 22|| 23|| 24|| 25|| 26|| 27|| 2d|| 2e||  2a    |   | 49|| 4a|| 4b|   | 53|| 67|| 54|| 55|
+---++---++---++---++---++---++---++---++---++---++---++---++---++--------+   +---++---++---+   +---++---++---++---+
+-----++---++---++---++---++---++---++---++---++---++---++---++---++------+   +---++---++---+   +---++---++---++---+
| 2b  || 14|| 1a|| 08|| 15|| 17|| 1c|| 18|| 0c|| 12|| 13|| 2f|| 30||  28  |   | 4c|| 4d|| 4e|   | 5f|| 60|| 61|| 56|
+-----++---++---++---++---++---++---++---++---++---++---++---++---+++     |   +---++---++---+   +---++---++---++---+
+-----+ +---++---++---++---++---++---++---++---++---++---++---++---+|     |                     +---++---++---++---+
| 39  | | 04|| 16|| 07|| 09|| 0a|| 0b|| 0d|| 0e|| 0f|| 33|| 34|| 32||     |                     | 5c|| 5d|| 5e|| 57|
+-----+ +---++---++---++---++---++---++---++---++---++---++---++---++-----+                     +---++---++---++---+
+----++---++---++---++---++---++---++---++---++---++---++---++------------+        +---+        +---++---++---++---+
|    || 64|| 1d|| 1b|| 06|| 19|| 05|| 11|| 10|| 36|| 37|| 38||            |        | 52|        | 59|| 5a|| 5b|| 58|
+----++---++---++---++---++---++---++---++---++---++---++---++------------+        +---+        +---++---++---+|   |
+-----++-----++-----++-------------------------------++-----++-----++-----+   +---++---++---+   +--------++---+|   |
|     ||     ||     ||             2c                ||     ||     ||     |   | 50|| 51|| 4f|   | 62     || 63||   |
+-----++-----++-----++-------------------------------++-----++-----++-----+   +---++---++---+   +--------++---++---+

*0 : Ctrl_l + Shift_l + K_40
*1 : Ctrl_l + Shift_l + K_08
*2 : Ctrl_l + Shift_l + K_80
*3 : Ctrl_l + Shift_l + K_04


*/

#endif

			// modifier
			EP1INBUF[0] = 0;
			// spare
			EP1INBUF[1] = 0;
			// keys pressed
			EP1INBUF[2] = 0;
			EP1INBUF[3] = 0;
			EP1INBUF[4] = 0;
			EP1INBUF[5] = 0;
			EP1INBUF[5] = 0;
			EP1INBUF[5] = 0;
			EP1INBC = 8;
			#else
			#error TYPE_???
			#endif
		}
	}
	if( !(EP1OUTCS & MSK(EPBUSY) ) )
	{
/*
		EP1INBUF[0] = key;
		EP1INBUF[1] = 0;
		EP1INBC = 2;
*/
		EP1OUTBC = 0;

	}
}

void reply(__xdata char *buf,int len)
{
	int donable,i;

	AUTOPTR1H = MSB(buf);
	AUTOPTR1L = LSB(buf);
	while(len)
	{
		donable = len;
		if(donable>64)
			donable=64;

		for(i=0;i<donable;i++)
		{
			EP0BUF[i]=XAUTODAT1;
			printf("%02x ",EP0BUF[i]);
		}
		EP0BCH = 0;
		SYNCDELAY;
		EP0BCL =  donable;    // arm the IN transfer
		SYNCDELAY;
		len -= donable;
		while(EP0CS & MSK(EPBUSY))
			;  // wait for it to go out and get ACK'd
	}
	printf("\r\n");
}

void getDescriptor()
{
	__xdata char* conf;
	int len;

	switch(SETUPDAT[3])
	{
		case GD_DEVICE:
			conf=(__xdata char*)&DeviceDscr;
			len=conf[0];
		break;
		case GD_DEVICE_QUALIFIER:
			conf=(__xdata char*)&DeviceQualDscr;
			len=conf[0];
		break;
		case GD_CONFIGURATION:
		{
			conf = FullSpeed ? (__xdata char*)&FullSpeedConfigDscr : (__xdata char*)&HighSpeedConfigDscr;
			conf[1]=DSCR_CONFIG;
			len = *(int*)(conf+2);
		}
		break;
		case GD_OTHER_SPEED_CONFIGURATION:
		{
			conf = FullSpeed ? (__xdata char*)&HighSpeedConfigDscr : (__xdata char*)&FullSpeedConfigDscr;
			conf[1]=DSCR_OTHERSPEED;
			len = *(int*)(conf+2);
		}
		break;
		case GD_STRING:
			switch(SETUPDAT[2])
			{
				case 0:
					conf=(__xdata char*)&StringDscr0;
					len=conf[0];
				break;
				case 1:
					conf=(__xdata char*)&StringDscr1;
					len=conf[0];
				break;
				case 2:
					conf=(__xdata char*)&StringDscr2;
					len=conf[0];
				break;
				case 3:
					conf=(__xdata char*)&StringDscr3;
					len=conf[0];
				break;
				case 4:
					conf=(__xdata char*)&StringDscr4;
					len=conf[0];
				break;
				default: // Invalid string index requested
					EP0CS = EP0CS SET(EPSTALL);      // Stall End Point 0
					return;
			}
		
		break;
		case GD_HID:     // Get-Descriptor: HID
			conf=(__xdata char*)&HIDDscr;
			len=conf[0];
		break;
		case GD_REPORT:  // Get-Descriptor: Report
		{
			conf=(__xdata char*)&ReportDscr;
			len=(int)&ReportDscrEnd - (int)&ReportDscr;
		}
		break;
		default:            // Invalid request
			printf("getDescriptor 0x%02x\r\n",SETUPDAT[3]);
			SetupData=SETUPDAT[1];
			EP0CS = EP0CS SET(EPSTALL);      // Stall End Point 0
			return;
	}

	if(len>*(int *)(SETUPDAT+6))
		len=*(int *)(SETUPDAT+6);

/*
	printf("%02x %02x %02x %02x %02x %02x %02x %02x\r\n",
		conf[0],conf[1],conf[2],conf[3],conf[4],conf[5],conf[6],conf[7]);
*/
	//printf("%02x %02x %02x %02x\r\n",conf[0],conf[1],conf[2],conf[3]);
	//printf("0x%04x 0x%04x 0x%04x\r\n",(int)conf,(int)&FullSpeedConfigDscr,(int)&HighSpeedConfigDscr);
	reply(conf,len);
	//SUDPTRH = MSB(conf);
	//SUDPTRL = LSB(conf);
//	printf("%02x %02x %02x %02x %02x\r\n",EP0CS,EP0BUF[0],EP0BUF[1],EP0BUF[2],EP0BUF[3]);
}

unsigned char Configuration;
void setConfiguration()
{
	printf("setConfiguration\r\n");
	Configuration = SETUPDAT[2];
	EP0BCH = 0;
	EP0BCL = 0;
}

void getConfiguration()
{
	EP0BUF[0] = Configuration;
	EP0BCH = 0;
	EP0BCL = 1;
}

void getInterface()
{
	EP0BUF[0] = 0;
	EP0BCH = 0;
	EP0BCL = 1;
}

void setFeature()
{
	printf("setFeature\r\n");

	switch(SETUPDAT[0])
	{
		case GS_DEVICE:
		break;
		case GS_INTERFACE:
		break;
		case GS_ENDPOINT:
			switch(SETUPDAT[4])
			{
				case 0x00:
					EP0CS = EP0CS SET(EPSTALL);
					TOGCTL = 0x00;
					TOGCTL = 0x20;
				break;
				case 0x80:
					EP0CS = EP0CS SET(EPSTALL);
					TOGCTL = 0x10;
					TOGCTL = 0x30;
				break;
				case 0x81:
					EP1INCS = EP1INCS SET(EPSTALL);
					TOGCTL = 0x11;
					TOGCTL = 0x31;
				break;
			}
		break;
	}
}

void clearFeature()
{
	printf("clearFeature\r\n");

	switch(SETUPDAT[0])
	{
		case GS_DEVICE:
		break;
		case GS_INTERFACE:
		break;
		case GS_ENDPOINT:
			switch(SETUPDAT[4])
			{
				case 0x00:
					TOGCTL = 0x00;
					TOGCTL = 0x20;
				break;
				case 0x80:
					TOGCTL = 0x10;
					TOGCTL = 0x30;
				break;
				case 0x81:
					EP1INCS = EP1INCS CLR(EPSTALL);
					TOGCTL = 0x11;
					TOGCTL = 0x31;
				break;
			}
		break;
		case 0xa1:
			EP0BUF[0] = 0;
			EP0BCH = 0;
			EP0BCL = 1;
		break;
	}
}

__xdata char * epcsIn[]=
{
	&EP0CS, &EP1INCS,  &EP2CS, &EP2CS, &EP4CS, &EP4CS, &EP6CS, &EP6CS, &EP8CS, &EP8CS
};

__xdata char * epcsOut[]=
{
	&EP0CS, &EP1OUTCS, &EP2CS, &EP2CS, &EP4CS, &EP4CS, &EP6CS, &EP6CS, &EP8CS, &EP8CS
};

void getStatus()
{
	switch(SETUPDAT[0])
	{
		case GS_DEVICE:            // Device
			EP0BUF[0] = 0;
			EP0BUF[1] = 0;
			EP0BCH = 0;
			EP0BCL = 2;
		break;
		case GS_INTERFACE:         // Interface
			EP0BUF[0] = 0;
			EP0BUF[1] = 0;
			EP0BCH = 0;
			EP0BCL = 2;
		break;
		case GS_ENDPOINT:         // End Point
			if(SETUPDAT[4]&0x80)
				EP0BUF[0] = *epcsIn [SETUPDAT[4]&0x7f] & MSK(EPSTALL);
			else
				EP0BUF[0] = *epcsOut[SETUPDAT[4]&0x7f] & MSK(EPSTALL);
			EP0BUF[1] = 0;
			EP0BCH = 0;
			EP0BCL = 2;
		break;
		default:            // Invalid Command
			printf("getStatus 0x%02x\r\n",SETUPDAT[0]);
			EP0CS = EP0CS SET(EPSTALL);
	}
}

void setupData()
{
	printf("setupData %02x %02x %02x %02x %02x %02x %02x %02x\r\n",
		SETUPDAT[0],SETUPDAT[1],SETUPDAT[2],SETUPDAT[3],SETUPDAT[4],SETUPDAT[5],SETUPDAT[6],SETUPDAT[7]);

	switch(SETUPDAT[1])
	{
		case SC_GET_STATUS:
			getStatus();
		break;
/*
		case SC_SET_ADDRESS:
			setAddress();
		break;
*/
		case SC_CLEAR_FEATURE:
			clearFeature();
		break;
		case SC_GET_DESCRIPTOR:
			getDescriptor();
		break;
		case SC_SET_CONFIGURATION:
			setConfiguration();
		break;
		case SC_GET_CONFIGURATION:
			getConfiguration();
		break;
		case SC_GET_INTERFACE:
			getInterface();
		break;
		default:
			printf("!! setupData 0x%02x\r\n",SETUPDAT[1]);
			EP0CS = EP0CS SET(EPSTALL);      // Stall End Point 0
	}
/*
	printf("%02x %02x %02x %02x %02x\r\n",
		EP0CS,EP0BUF[0],EP0BUF[1],EP0BUF[2],EP0BUF[3]);
	printf("%02x %02x %02x %02x %02x\r\n",
		EP0CS,EP0BUF[0],EP0BUF[1],EP0BUF[2],EP0BUF[3]);
	printf("%02x %02x %02x %02x %02x\r\n",
		EP0CS,EP0BUF[0],EP0BUF[1],EP0BUF[2],EP0BUF[3]);
	printf("%02x %02x %02x %02x %02x\r\n",
		EP0CS,EP0BUF[0],EP0BUF[1],EP0BUF[2],EP0BUF[3]);
	printf("%02x %02x %02x %02x %02x\r\n",
		EP0CS,EP0BUF[0],EP0BUF[1],EP0BUF[2],EP0BUF[3]);
*/

	EP0CS = EP0CS SET(HSNAK);
}

#define USB_AUTO_VECS \
	USB_AUTO_VEC(0x00,SUDAV) \
	USB_AUTO_VEC(0x04,SOF) \
	USB_AUTO_VEC(0x08,SUTOK) \
	USB_AUTO_VEC(0x0C,SUSPEND) \
	USB_AUTO_VEC(0x10,RESET) \
	USB_AUTO_VEC(0x14,HISPEED) \
	USB_AUTO_VEC(0x18,EP0ACK) \
	USB_AUTO_VEC(0x1C,RESERVED) \
	USB_AUTO_VEC(0x20,EP0IN) \
	USB_AUTO_VEC(0x24,EP0OUT) \
	USB_AUTO_VEC(0x28,EP1IN) \
	USB_AUTO_VEC(0x2C,EP1OUT) \
	USB_AUTO_VEC(0x30,EP2INOUT) \
	USB_AUTO_VEC(0x34,EP4INOUT) \
	USB_AUTO_VEC(0x38,EP6INOUT) \
	USB_AUTO_VEC(0x3C,EP8INOUT) \
	USB_AUTO_VEC(0x40,IBN) \
	USB_AUTO_VEC(0x44,RESERVED) \
	USB_AUTO_VEC(0x48,EP0PING) \
	USB_AUTO_VEC(0x4C,EP1PING) \
	USB_AUTO_VEC(0x50,EP2PING) \
	USB_AUTO_VEC(0x54,EP4PING) \
	USB_AUTO_VEC(0x58,EP6PING) \
	USB_AUTO_VEC(0x5C,EP8PING) \
	USB_AUTO_VEC(0x60,ERRLIMIT) \
	USB_AUTO_VEC(0x64,RESERVED) \
	USB_AUTO_VEC(0x68,RESERVED) \
	USB_AUTO_VEC(0x6C,RESERVED) \
	USB_AUTO_VEC(0x70,EP2ISOERR) \
	USB_AUTO_VEC(0x74,EP4ISOERR) \
	USB_AUTO_VEC(0x78,EP6ISOERR) \
	USB_AUTO_VEC(0x7C,EP8ISOERR) \
	USB_AUTO_VEC(0x80,EP2PF) \
	USB_AUTO_VEC(0x84,EP4PF) \
	USB_AUTO_VEC(0x88,EP6PF) \
	USB_AUTO_VEC(0x8C,EP8PF) \
	USB_AUTO_VEC(0x90,EP2EF) \
	USB_AUTO_VEC(0x94,EP4EF) \
	USB_AUTO_VEC(0x98,EP6EF) \
	USB_AUTO_VEC(0x9C,EP8EF) \
	USB_AUTO_VEC(0xA0,EP2FF) \
	USB_AUTO_VEC(0xA4,EP4FF) \
	USB_AUTO_VEC(0xA8,EP6FF) \
	USB_AUTO_VEC(0xAC,EP8FF) \
	USB_AUTO_VEC(0xB0,GPIFDONE) \
	USB_AUTO_VEC(0xB4,GPIFWF) \
/**/

void UsbIsrSUDAV(void) __interrupt
{
	Debug|=4;
	GotSUD = 1;
	EXIF = EXIF CLR(USBNT);
	USBIRQ = MSK(SUDAV);
}

// Setup Token Interrupt Handler
void UsbIsrSUTOK(void) __interrupt
{
	EXIF = EXIF CLR(USBNT);
	USBIRQ = MSK(SUTOK);
}

void UsbIsrSOF(void) __interrupt
{
	EXIF = EXIF CLR(USBNT);
	USBIRQ = MSK(SOF);
}

void UsbIsrRESET(void) __interrupt
{
	// whenever we get a USB reset, we should revert to full speed mode
	FullSpeed=1;
/*
	pConfigDscr = pFullSpeedConfigDscr;
	((CONFIGDSCR xdata *) pConfigDscr)->type = DSCR_CONFIG;
	pOtherConfigDscr = pHighSpeedConfigDscr;
	((CONFIGDSCR xdata *) pOtherConfigDscr)->type = DSCR_OTHERSPEED;
*/
	Debug|=1;
	EXIF = EXIF CLR(USBNT);
	USBIRQ = MSK(URES);
}

void UsbIsrSUSPEND(void) __interrupt
{
	Debug|=8;
	Sleep = 1;
	EXIF = EXIF CLR(USBNT);
	USBIRQ = MSK(SUSP);
}

void UsbIsrHISPEED(void) __interrupt
{
	Debug|=2;
	if( USBCS & MSK(HSM) )
	{
		FullSpeed=0;
	}
	
	EXIF = EXIF CLR(USBNT);
	USBIRQ = MSK(HSGRANT);
}

void UsbIsrEP0ACK(void) __interrupt
{
}

void UsbIsrSTUB(void) __interrupt
{
}

void UsbIsrEP0IN(void) __interrupt
{
}

void UsbIsrEP0OUT(void) __interrupt
{
}

void UsbIsrEP1IN(void) __interrupt
{
}

void UsbIsrEP1OUT(void) __interrupt
{
}

void UsbIsrEP2INOUT(void) __interrupt
{
}

void UsbIsrEP4INOUT(void) __interrupt
{
}

void UsbIsrEP6INOUT(void) __interrupt
{
}

void UsbIsrEP8INOUT(void) __interrupt
{
}

void UsbIsrIBN(void) __interrupt
{
}

void UsbIsrEP0PING(void) __interrupt
{
}

void UsbIsrEP1PING(void) __interrupt
{
}

void UsbIsrEP2PING(void) __interrupt
{
}

void UsbIsrEP4PING(void) __interrupt
{
}

void UsbIsrEP6PING(void) __interrupt
{
}

void UsbIsrEP8PING(void) __interrupt
{
}

void UsbIsrERRLIMIT(void) __interrupt
{
}

void UsbIsrEP2ISOERR(void) __interrupt
{
}

void UsbIsrEP4ISOERR(void) __interrupt
{
}

void UsbIsrEP6ISOERR(void) __interrupt
{
}

void UsbIsrEP8ISOERR(void) __interrupt
{
}

void UsbIsrEP2PF(void) __interrupt
{
}

void UsbIsrEP4PF(void) __interrupt
{
}

void UsbIsrEP6PF(void) __interrupt
{
}

void UsbIsrEP8PF(void) __interrupt
{
}

void UsbIsrEP2EF(void) __interrupt
{
}

void UsbIsrEP4EF(void) __interrupt
{
}

void UsbIsrEP6EF(void) __interrupt
{
}

void UsbIsrEP8EF(void) __interrupt
{
}

void UsbIsrEP2FF(void) __interrupt
{
}

void UsbIsrEP4FF(void) __interrupt
{
}

void UsbIsrEP6FF(void) __interrupt
{
}

void UsbIsrEP8FF(void) __interrupt
{
}

void UsbIsrGPIFDONE(void) __interrupt
{
}

void UsbIsrGPIFWF(void) __interrupt
{
}

void UsbIsrRESERVED(void) __interrupt
{
}

void go_abs() __naked
{
	__asm
	.area usbInterruptsTable    (ABS)
	.org 0x1f00;
	__endasm;
}

void USB_interrupt() __interrupt 8 __naked
{
#define USB_AUTO_VEC(address,name) __asm ljmp _UsbIsr ## name __endasm ;__asm nop __endasm ;
	USB_AUTO_VECS
#undef USB_AUTO_VEC
	__asm
	.area CSEG    (CODE)
	__endasm;
}

