#if 0

proc build { {selection {}} } {
	file mkdir .build
	cd .build
	exec sdcc -mmcs51 ../fast800.c -c
	exec sdcc fast800.rel
}

proc loadAndRun { } {
	ez::reset 1
	ez::loadIhex .build/fast800.ihx
	ez::reset 0
}

proc Debug { } {
	puts stdout [format "0x%08x" [ez::r 0x1ffc] ]
}

proc SetupData { } {
	puts stdout [format "0x%08x" [ez::r 0x1ff8] ]
}

proc go { {selection loadAndRun } } {
	source ../tcl/ez.tcl
	ez::open
	$selection
	ez::close
}

proc clean {} {
	exec rm *.map *.ihx *.asm *.lnk *.lst *.mem *.rel *.rst *.sym
}

puts stdout $argv
eval $argv

return

// http://www.danielclemente.com/placa_pi/include-fix/fx2sdly.h

#endif

#include "ez-fx2.h"
#include "stdio.h"

xdata at 0x1ffc char Debug;


const char far * outs[]= { (const char far *)0x7f96, (const char far *)0x7f97, (const char far *)0x7f98, (const char far *)0x7841, (const char far *)0x7845, };
const char far *pinss[]= { (const char far *)0x7f99, (const char far *)0x7f9a, (const char far *)0x7f9b, (const char far *)0x7842, (const char far *)0x7846, };
const char far *  oes[]= { (const char far *)0x7f9c, (const char far *)0x7f9d, (const char far *)0x7f9e, (const char far *)0x7843, (const char far *)0x7847, };

// http://castet.matthieu.free.fr/eagle/usb/memory_after_booting
#if 0
7840: ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff 
7f90: d7 7a 01 e2 97 86 bf 99 db e9 7c 2d e1 74 f8 20 
#endif

// http://castet.matthieu.free.fr/eagle/usb/memory_at_init
#if 0
7840: 80 99 b8 24 c0 a6 e6 da a4 f0 a9 20 d6 a2 b2 24 
7f90: d7 7a 01 e2 97 86 bf 99 db e9 7c 2d e1 74 f8 20 
#endif

void org_init()
{
	_asm

#if 0
	mov   0x2b,A
	mov   0x2b,0x80
	mov   0x45,#0x80
#endif

	/* 0d92 78 80           */ MOV   R0,#0x80
	/* 0d94 76 88           */ MOV   @R0,#0x88
	/* 0d96 e6              */ MOV   A,@R0
	/* 0d97 f5 b2           */ MOV   0xb2,A
	/* 0d99 08              */ INC   R0
	/* 0d9a 76 08           */ MOV   @R0,#0x08
	/* 0d9c e6              */ MOV   A,@R0
	/* 0d9d f5 b3           */ MOV   0xb3,A
	/* 0d9f 08              */ INC   R0
	/* 0da0 76 e4           */ MOV   @R0,#0xe4
	/* 0da2 e6              */ MOV   A,@R0
	/* 0da3 f5 b4           */ MOV   0xb4,A
	/* 0da5 e6              */ MOV   A,@R0
	/* 0da6 44 10           */ ORL   A,#0x10
	/* 0da8 f6              */ MOV   @R0,A
	/* 0da9 e6              */ MOV   A,@R0
	/* 0daa 90 7f 9e        */ MOV   DPTR,#0x7f9e
	/* 0dad f0              */ MOVX  @DPTR,A
	/* 0dae e6              */ MOV   A,@R0
	/* 0daf 54 df           */ ANL   A,#0xdf
	/* 0db1 f6              */ MOV   @R0,A
	/* 0db2 e6              */ MOV   A,@R0
	/* 0db3 f0              */ MOVX  @DPTR,A
	/* 0db4 08              */ INC   R0
	/* 0db5 76 80           */ MOV   @R0,#0x80
	/* 0db7 e6              */ MOV   A,@R0
	/* 0db8 f5 b5           */ MOV   0xb5,A
	/* 0dba e4              */ CLR   A
	/* 0dbb f5 2c           */ MOV   0x2c,A
	/* 0dbd 12 10 00        */ LCALL label_1000
	/* 0dc0 53 2c ef        */ ANL   0x2c,#0xef
	/* 0dc3 85 2c 90        */ MOV   0x90,0x2c
	/* 0dc6 75 2d 01        */ MOV   0x2d,#0x01
	/* 0dc9 85 2d a0        */ MOV   0xa0,0x2d
	/* 0dcc 53 2d fe        */ ANL   0x2d,#0xfe
	/* 0dcf 85 2d a0        */ MOV   0xa0,0x2d
	/* 0dd2 53 2d fd        */ ANL   0x2d,#0xfd
	/* 0dd5 85 2d a0        */ MOV   0xa0,0x2d
	/* 0dd8 53 2d fb        */ ANL   0x2d,#0xfb
	/* 0ddb 12 10 72        */ LCALL label_1072
	/* 0dde 53 2d ef        */ ANL   0x2d,#0xef
	/* 0de1 85 2d a0        */ MOV   0xa0,0x2d

	_endasm;
}

void wait(int delay)
{
	char delay0;
	char delay1;

	delay0=delay&0xff;
	delay1=delay>>8;
	do
	{
		do
		{
		}
		while(--delay0);
	}
	while(--delay1);
}

int main()
{
#define bit 2

#define OE OEE
#define IO IOE

	//init_org();

#if 0
	char b;

	OE = OE SET(BIT(bit));
	b=0;
	while(1)
	{
		if(Debug&1)
			b = b CLR(BIT(bit));
		else
			b = b SET(BIT(bit));
		IO = b;

		Debug++;
		delay(0);
	}

#endif

	int delay=0;
	char k=0;
	while(1)
	{
		char i;
		for(i=0;i<5;i++)
		{
			char far *oe=oes[3];
			char far *out=outs[3];
			char j;
			for(j=5;j<6;j++)
			{
				*oe  = *oe SET(BIT(j));
				*out = *out SET(BIT(j));
				wait(delay);
				*out = *out CLR(BIT(j));
				wait(delay);
				*oe  = *oe CLR(BIT(j));
			}
		}
	}

}


void label_1072() _naked
{
	_asm

label_1072:
	/* 1072 85 2d a0        */ MOV   0xa0,0x2d
	/* 1075 53 2d f7        */ ANL   0x2d,#0xf7
	/* 1078 85 2d a0        */ MOV   0xa0,0x2d
	/* 107b 22              */ RET   

	_endasm;
}

void labe_1000() _naked
{
	_asm

label_1000:
	/* 1000 85 2c 90        */ MOV   0x90,0x2c
label_1003:
	/* 1003 43 2c 04        */ ORL   0x2c,#0x04
	/* 1006 85 2c 90        */ MOV   0x90,0x2c
	/* 1009 53 2c fb        */ ANL   0x2c,#0xfb
	/* 100c 85 2c 90        */ MOV   0x90,0x2c
	/* 100f 43 2c 08        */ ORL   0x2c,#0x08
	/* 1012 85 2c 90        */ MOV   0x90,0x2c
	/* 1015 53 2c f7        */ ANL   0x2c,#0xf7
	/* 1018 85 2c 90        */ MOV   0x90,0x2c
	/* 101b 22              */ RET   

	_endasm;
}
