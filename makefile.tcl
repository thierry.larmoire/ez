
# wget http://www.lvr.com/files/fx2hid.zip

cd driver
exec make -C /usr/src/linux M=[pwd] modules >@stdout 2>@stderr

return

acroread /mnt/windows/K/documentations/electronics/usb/cy7c68013_5.pdf &


set cc gcc
set ld ld
set cflags [list -I /usr/linux/include -D CPU=686]
set mod_flags [concat $cflags [list -D __KERNEL__ -D MODULE] ]
set app_flags [concat $cflags ]

set mods cy7c8013

foreach mod $mods {
	lappend objs $mod.o
}

set objDir objs
set mods [list cy7c68013]

proc depend { left rights make } {
	set doit 0
	if { ![file exists $left] } {
		#puts stdout "[pwd]/$left <- $rights n"
		incr doit
	} {
		foreach right $rights {
			if { [file mtime $left]<=[file mtime $right] } {
				#puts stdout "$left <- $rights d"
				incr doit
				break
			}
		}
	}
	if { $doit } {
		uplevel $make
	}
}

proc vexec { args } {
}

foreach mod $mods {
	set o $objDir/$mod.o
	set c $mod.c
	depend $o [list $c] {
		set cmd [concat exec $cc $mod_flags $c -c -o $o >&@stdout]
		puts stdout [join $cmd]
		eval $cmd
	}
}
