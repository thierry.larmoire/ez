
load tcl/ez.so

proc ez::load { $file } {
	set buf [binary format a0 ""]
	
	set fd [open ../ez51.ihx]
	while { [gets $fd line]!=-1} {
		scan $line {:%2x%4x%2x} count address type
		if { $type==0 } {
			set hex [string range $line 9 [expr 8+2*$count]]
			scan [string range $line [expr 9+$count-1] [expr 9+$count+1]] {%2x} chksum
			set data [binary format H* $hex]
			#puts stdout [format "%d 0x%04x %d %s" $type $address $count $hex]
			set buf [binary format a*@[set address]a* $buf $data]
		}
	}
	close $fd
	
	#puts stdout [string length $buf]
	#binary scan $buf H* hex
	#puts stdout $hex

	
	#binary scan $bytes c* bytes
	#foreach byte $bytes {
	#	puts stdout [format 0x%02x [expr $byte&0xff]]
	#}
	
	set code [binary format c* {
		0xa6 0x00
		0x00 0x00 0x00 0x00
		0x78 0xaa
		0x75 0xb1 0x45
		0x02 0x00 0x04
		0x76 0x22
		0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00
		0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00
		0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00
	}]
	
	binary scan $buf i* dwords
	set i 0
	foreach dword $dwords {
		#puts stdout [format 0x%08x $dword]
		ez::w $i $dword
		incr i 4
	}
}

proc ez::dump { from to } {
	set dwords [list]
	for { set i $from } { $i<$to } { incr i 4 } {
		lappend dwords [ez::r [expr 0x0000+$i]]
	}
	set bytes [binary format i* $dwords]
	binary scan $bytes H* hex
	set hex
}

ez::open
ez::reset 1

ez::reset 0

ez::close


proc load { file } {
}
