#if 0

#puts stdout [pwd]
#exec 2>@stderr >@stdout make -C /usr/src/linux-headers-[exec uname -r] M=[pwd] modules
#exit
set path [file dir [info script]]
set build $path/.build
file mkdir $build
close [open $build/Makefile {WRONLY CREAT TRUNC}]

set kernel /usr/src/linux-headers-[exec uname -r]
set module [file normalize $path]
set output [file normalize $build]
exec 2>@stderr >@stdout make -C $kernel M=$output src=$module modules

return
#endif
// SPDX-License-Identifier: GPL-2.0
/*
 * USB ez driver - 0.3
 * Copyright (C) 2005 Thierry Larmoire (thierry.larmoire@free.fr)
 *
 * based on /usr/src/linux/drivers/usb/usb-skeleton.c {
 *
 * USB Skeleton driver - 2.2
 *
 * Copyright (C) 2001-2004 Greg Kroah-Hartman (greg@kroah.com)
 *
 * }, thanks.
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation, version 2.
 * 
 */

#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/kref.h>
#include <linux/uaccess.h>
#include <linux/usb.h>
#include <linux/mutex.h>
#include "ez.h"

/*
	philips mobiphone base
	xtal 12MHz
	cypress NY0738 AN2122SC F0016
	24LC01B
*/

/*
	adaptec avc2200 mpeg2 video cap
	xtal 24MHz
	cypress cy7c68013
	24LC??
*/

#define USB_DEVICE_SUPPORTEDS \
	/*                  (vendor,device,t,"comment")     t is type : an (0), fx(1), fx2(2) */  \
	USB_DEVICE_SUPPORTED(0x0471,0x080b,0,"philips mobiphone base (fisio ...)") \
	USB_DEVICE_SUPPORTED(0x03f3,0x0083,2,"adaptec avc2200 mpeg2 video cap") \
	USB_DEVICE_SUPPORTED(0x0eb1,0x7007,2,"WIS 7007 eval board") \
	USB_DEVICE_SUPPORTED(0x1110,0x9022,1,"EAGLE II, adsl modem") \
	USB_DEVICE_SUPPORTED(0x04b4,0x8613,2,"EZ-USB FX2LP CY7C68013A") \
/**/

/* table of devices that work with this driver */
static const struct usb_device_id ez_table [] = {
	#define USB_DEVICE_SUPPORTED(vendor,device,type,comment) { USB_DEVICE(vendor, device) },
		USB_DEVICE_SUPPORTEDS
	#undef USB_DEVICE_SUPPORTED
	{ }					/* Terminating entry */
};
MODULE_DEVICE_TABLE (usb, ez_table);

static int ez_types[]=
{
	#define USB_DEVICE_SUPPORTED(vendor,device,type,comment) type,
		USB_DEVICE_SUPPORTEDS
	#undef USB_DEVICE_SUPPORTED
};

/* Get a minor range for your devices from the usb maintainer */
#define USB_EZ_MINOR_BASE	192

/* our private defines. if this grows any larger, use your own .h file */
#define MAX_TRANSFER		(PAGE_SIZE - 512)
/*
 * MAX_TRANSFER is chosen so that the VM is not stressed by
 * allocations > PAGE_SIZE and the number of packets in a page
 * is an integer 512 is the largest possible packet on EHCI
 */
#define WRITES_IN_FLIGHT	8
/* arbitrarily chosen */

/* Structure to hold all of our device specific stuff */
struct usb_ez {
	struct usb_device    * udev;                /* the usb device for this device */
	struct usb_interface * interface;           /* the interface for this device */
	struct semaphore     limit_sem;		/* limiting the number of writes in progress */
	struct usb_anchor    submitted;		/* in case we need to retract our submissions */
	struct urb           *bulk_in_urb;		/* the urb to read data with */
	unsigned char        *bulk_in_buffer;        /* the buffer to receive data */
	size_t               bulk_in_size;          /* the size of the receive buffer */
	size_t               bulk_in_filled;		/* number of bytes in the buffer */
	size_t               bulk_in_copied;		/* already copied to user space */
	__u8                 bulk_in_endpointAddr;  /* the address of the bulk in endpoint */
	__u8                 bulk_out_endpointAddr; /* the address of the bulk out endpoint */
	int			errors;			/* the last request tanked */
	bool			ongoing_read;		/* a read is going on */
	spinlock_t		err_lock;		/* lock for errors */
	struct kref          kref;
	struct mutex         io_mutex;		/* synchronize I/O with disconnect */
	unsigned long        disconnected:1;
	wait_queue_head_t    bulk_in_wait;		/* to wait for an ongoing read */
	int type;
};
/*
 * These are the requests (bRequest) that the bootstrap loader is expected
 * to recognize.  The codes are reserved by Cypress, and these values match
 * what EZ-USB hardware, or "Vend_Ax" firmware (2nd stage loader) uses.
 * Cypress' "a3load" is nice because it supports both FX and FX2, although
 * it doesn't have the EEPROM support (subset of "Vend_Ax").
 */
#define RW_INTERNAL      0xA0  /* hardware implements this one */
#define RW_EEPROM        0xA2
#define RW_MEMORY        0xA3
#define GET_EEPROM_SIZE  0xA5

static int ez_internal_access(struct usb_ez *dev,int to,__u16 address, void *data, __u16 size)
{
	int done;
	done = usb_control_msg(
		dev->udev,
		( to ? usb_sndctrlpipe(dev->udev, 0):usb_rcvctrlpipe(dev->udev, 0) ),
		RW_INTERNAL,
		(to ? USB_DIR_OUT:USB_DIR_IN) | USB_TYPE_VENDOR | USB_RECIP_DEVICE,
		address,
		0,
		data,
		size,
		HZ
	);
	return done;
}

inline static int ez_internal_read(struct usb_ez *dev, __u16 address, void *data, __u16 size)
{
	return ez_internal_access(dev,0,address,data,size);
}

inline static int ez_internal_write(struct usb_ez *dev, __u16 address, void *data, __u16 size)
{
	return ez_internal_access(dev,1,address,data,size);
}

static int ez_cpus(struct usb_ez *dev,__u8 reset)
{
	int done;

	done = ez_internal_write(
		dev,
		dev->type==2 ? 0xe600:0x7f92,
		&reset,
		sizeof(reset)
	);

	pr_info("reset %d %s\n", reset, done ? "done":"failed");

	return done;
}

#define to_ez_dev(d) container_of(d, struct usb_ez, kref)

static struct usb_driver ez_driver;
static void ez_draw_down(struct usb_ez *dev);

static void ez_delete(struct kref *kref)
{
	struct usb_ez *dev = to_ez_dev(kref);

	usb_free_urb(dev->bulk_in_urb);
	usb_put_intf(dev->interface);
	usb_put_dev(dev->udev);
	kfree (dev->bulk_in_buffer);
	kfree (dev);
}

static int ez_open(struct inode *inode, struct file *file)
{
	struct usb_ez *dev;
	struct usb_interface *interface;
	int subminor;
	int retval = 0;

	subminor = iminor(inode);

	interface = usb_find_interface(&ez_driver, subminor);
	if (!interface) {
		pr_err("%s - error, can't find device for minor %d\n",
			__func__, subminor);
		retval = -ENODEV;
		goto exit;
	}

	dev = usb_get_intfdata(interface);
	if (!dev) {
		retval = -ENODEV;
		goto exit;
	}

	retval = usb_autopm_get_interface(interface);
	if (retval)
		goto exit;

	/* increment our usage count for the device */
	kref_get(&dev->kref);

	/* save our object in the file's private structure */
	file->private_data = dev;

exit:
	return retval;
}

static int ez_release(struct inode *inode, struct file *file)
{
	struct usb_ez *dev;

	dev = file->private_data;
	if (dev == NULL)
		return -ENODEV;

	/* allow the device to be autosuspended */
	usb_autopm_put_interface(dev->interface);

	/* decrement the count on our device */
	kref_put(&dev->kref, ez_delete);
	return 0;
}

#define IOCTL_FUNCTION(_name,_type) \
	int _name (struct usb_ez *dev, unsigned int cmd, _type *arg,char *name)

typedef IOCTL_FUNCTION(IoctlFunction,void);

#define ON_IOCTL(_name) \
	static IOCTL_FUNCTION(ez_ioctl_ ## _name,ez_ioctl_type_ ## _name)
#define ON_IOCTL_ALIAS(from,to) \
	ON_IOCTL(to) __attribute__ ((unused, alias(__stringify(ez_ioctl_ ## from))))

#define EZ_IOCTL(r,_name,type) \
	ON_IOCTL(_name);
EZ_IOCTLS
#undef EZ_IOCTL

static IoctlFunction* ez_ioctls[]=
{
	#define EZ_IOCTL(r,name,type) (IoctlFunction*)ez_ioctl_ ## name,
	EZ_IOCTLS
	#undef EZ_IOCTL
};

static long ez_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	struct usb_ez *dev;
	IoctlFunction *function;
	unsigned nr=_IOC_NR(cmd);
	unsigned dir=_IOC_DIR(cmd);
	unsigned size=_IOC_SIZE(cmd);
	int err;
	union
	{
		#define EZ_IOCTL(r,name,type) type name;
		EZ_IOCTLS
		#undef EZ_IOCTL
	}buf;

	if(nr>= ARRAY_SIZE(ez_ioctls))
		return -ENOIOCTLCMD;

	function=ez_ioctls[nr];

	dev = (struct usb_ez *)file->private_data;

	if(dir&_IOC_WRITE)
	{
		if(copy_from_user(&buf, (void __user *)arg, size)!=0)
			return -EFAULT;
	}

	err=(*function)(dev,cmd,&buf,"");
	if(err<0)
		return err;

	if(dir&_IOC_READ)
	{
		if(copy_to_user ((void __user *)arg, &buf, size) != 0)
			return -EFAULT;
	}

	return err;
}

ON_IOCTL(SET_CPU_RESET)
{
	__u8 reset=*arg;
	ez_cpus(dev,reset);
	return 0;
}

ON_IOCTL(GET_CPU_RESET)
{
	*arg=0;
	return 0;
}

ON_IOCTL(INTERNAL_WRITE)
{
	unsigned done;
	__u8 buf[64];
	int to;

	if(arg->size>=sizeof(buf))
		arg->size=sizeof(buf);

	to = cmd==EZ_INTERNAL_WRITE ? 1:0 ;

	if(to)
	{
		if(copy_from_user (buf, (void __user *)arg->data, arg->size) != 0)
			return -EFAULT;
	}

	done= ez_internal_access(dev, to, arg->address, buf, arg->size);

	if(!to)
	{
		if(copy_to_user ((void __user *)arg->data, buf, done) != 0)
			return -EFAULT;
	}

	return done!=arg->size ? EIO : done;
}

ON_IOCTL_ALIAS(INTERNAL_WRITE,INTERNAL_READ);

static int ez_flush(struct file *file, fl_owner_t id)
{
	struct usb_ez *dev;
	int res;

	dev = file->private_data;
	if (dev == NULL)
		return -ENODEV;

	/* wait for io to stop */
	mutex_lock(&dev->io_mutex);
	ez_draw_down(dev);

	/* read out errors, leave subsequent opens a clean slate */
	spin_lock_irq(&dev->err_lock);
	res = dev->errors ? (dev->errors == -EPIPE ? -EPIPE : -EIO) : 0;
	dev->errors = 0;
	spin_unlock_irq(&dev->err_lock);

	mutex_unlock(&dev->io_mutex);

	return res;
}

static void ez_read_bulk_callback(struct urb *urb)
{
	struct usb_ez *dev;
	unsigned long flags;

	dev = urb->context;

	spin_lock_irqsave(&dev->err_lock, flags);
	/* sync/async unlink faults aren't errors */
	if (urb->status) {
		if (!(urb->status == -ENOENT ||
		    urb->status == -ECONNRESET ||
		    urb->status == -ESHUTDOWN))
			dev_err(&dev->interface->dev,
				"%s - nonzero write bulk status received: %d\n",
				__func__, urb->status);

		dev->errors = urb->status;
	} else {
		dev->bulk_in_filled = urb->actual_length;
	}
	dev->ongoing_read = 0;
	spin_unlock_irqrestore(&dev->err_lock, flags);

	wake_up_interruptible(&dev->bulk_in_wait);
}

static int ez_do_read_io(struct usb_ez *dev, size_t count)
{
	int rv;

	/* prepare a read */
	usb_fill_bulk_urb(dev->bulk_in_urb,
			dev->udev,
			usb_rcvbulkpipe(dev->udev,
				dev->bulk_in_endpointAddr),
			dev->bulk_in_buffer,
			min(dev->bulk_in_size, count),
			ez_read_bulk_callback,
			dev);
	/* tell everybody to leave the URB alone */
	spin_lock_irq(&dev->err_lock);
	dev->ongoing_read = 1;
	spin_unlock_irq(&dev->err_lock);

	/* submit bulk in urb, which means no data to deliver */
	dev->bulk_in_filled = 0;
	dev->bulk_in_copied = 0;

	/* do it */
	rv = usb_submit_urb(dev->bulk_in_urb, GFP_KERNEL);
	if (rv < 0) {
		dev_err(&dev->interface->dev,
			"%s - failed submitting read urb, error %d\n",
			__func__, rv);
		rv = (rv == -ENOMEM) ? rv : -EIO;
		spin_lock_irq(&dev->err_lock);
		dev->ongoing_read = 0;
		spin_unlock_irq(&dev->err_lock);
	}

	return rv;
}

static ssize_t ez_read(struct file *file, char *buffer, size_t count, loff_t *ppos)
{
	struct usb_ez *dev;
	int rv;
	bool ongoing_io;

	dev = file->private_data;
	
	if (!count)
		return 0;

	/* no concurrent readers */
	rv = mutex_lock_interruptible(&dev->io_mutex);
	if (rv < 0)
		return rv;

	if (dev->disconnected) {		/* disconnect() was called */
		rv = -ENODEV;
		goto exit;
	}

	/* if IO is under way, we must not touch things */
retry:
	spin_lock_irq(&dev->err_lock);
	ongoing_io = dev->ongoing_read;
	spin_unlock_irq(&dev->err_lock);

	if (ongoing_io) {
		/* nonblocking IO shall not wait */
		if (file->f_flags & O_NONBLOCK) {
			rv = -EAGAIN;
			goto exit;
		}
		/*
		 * IO may take forever
		 * hence wait in an interruptible state
		 */
		rv = wait_event_interruptible(dev->bulk_in_wait, (!dev->ongoing_read));
		if (rv < 0)
			goto exit;
	}

	/* errors must be reported */
	rv = dev->errors;
	if (rv < 0) {
		/* any error is reported once */
		dev->errors = 0;
		/* to preserve notifications about reset */
		rv = (rv == -EPIPE) ? rv : -EIO;
		/* report it */
		goto exit;
	}

	/*
	 * if the buffer is filled we may satisfy the read
	 * else we need to start IO
	 */

	if (dev->bulk_in_filled) {
		/* we had read data */
		size_t available = dev->bulk_in_filled - dev->bulk_in_copied;
		size_t chunk = min(available, count);

		if (!available) {
			/*
			 * all data has been used
			 * actual IO needs to be done
			 */
			rv = ez_do_read_io(dev, count);
			if (rv < 0)
				goto exit;
		else
				goto retry;
		}
		/*
		 * data is available
		 * chunk tells us how much shall be copied
		 */

		if (copy_to_user(buffer,
				 dev->bulk_in_buffer + dev->bulk_in_copied,
				 chunk))
			rv = -EFAULT;
		else
			rv = chunk;

		dev->bulk_in_copied += chunk;

		/*
		 * if we are asked for more than we have,
		 * we start IO but don't wait
		 */
		if (available < count)
			ez_do_read_io(dev, count - chunk);
	} else {
		/* no data in the buffer */
		rv = ez_do_read_io(dev, count);
		if (rv < 0)
			goto exit;
		else
			goto retry;
	}
exit:
	mutex_unlock(&dev->io_mutex);
	return rv;
}

static void ez_write_bulk_callback(struct urb *urb)
{
	struct usb_ez *dev;
	unsigned long flags;

	dev = urb->context;

	/* sync/async unlink faults aren't errors */
	if (urb->status) {
		if (!(urb->status == -ENOENT ||
		    urb->status == -ECONNRESET ||
		    urb->status == -ESHUTDOWN))
			dev_err(&dev->interface->dev,
				"%s - nonzero write bulk status received: %d\n",
				__func__, urb->status);

		spin_lock_irqsave(&dev->err_lock, flags);
		dev->errors = urb->status;
		spin_unlock_irqrestore(&dev->err_lock, flags);
	}

	/* free up our allocated buffer */
	usb_free_coherent(urb->dev, urb->transfer_buffer_length,
			  urb->transfer_buffer, urb->transfer_dma);
	up(&dev->limit_sem);
}

static ssize_t ez_write(struct file *file, const char *user_buffer, size_t count, loff_t *ppos)
{
	struct usb_ez *dev;
	int retval = 0;
	struct urb *urb = NULL;
	char *buf = NULL;
	size_t writesize = min(count, (size_t)MAX_TRANSFER);

	dev = file->private_data;

	/* verify that we actually have some data to write */
	if (count == 0)
		goto exit;

	/*
	 * limit the number of URBs in flight to stop a user from using up all
	 * RAM
	 */
	if (!(file->f_flags & O_NONBLOCK)) {
		if (down_interruptible(&dev->limit_sem)) {
			retval = -ERESTARTSYS;
			goto exit;
		}
	} else {
		if (down_trylock(&dev->limit_sem)) {
			retval = -EAGAIN;
			goto exit;
		}
	}

	spin_lock_irq(&dev->err_lock);
	retval = dev->errors;
	if (retval < 0) {
		/* any error is reported once */
		dev->errors = 0;
		/* to preserve notifications about reset */
		retval = (retval == -EPIPE) ? retval : -EIO;
	}
	spin_unlock_irq(&dev->err_lock);
	if (retval < 0)
		goto error;

	/* create a urb, and a buffer for it, and copy the data to the urb */
	urb = usb_alloc_urb(0, GFP_KERNEL);
	if (!urb) {
		retval = -ENOMEM;
		goto error;
	}

	buf = usb_alloc_coherent(dev->udev, writesize, GFP_KERNEL,
				 &urb->transfer_dma);
	if (!buf) {
		retval = -ENOMEM;
		goto error;
	}

	if (copy_from_user(buf, user_buffer, writesize)) {
		retval = -EFAULT;
		goto error;
	}

	/* this lock makes sure we don't submit URBs to gone devices */
	mutex_lock(&dev->io_mutex);
	if (dev->disconnected) {		/* disconnect() was called */
		mutex_unlock(&dev->io_mutex);
		retval = -ENODEV;
		goto error;
	}

	/* initialize the urb properly */
	usb_fill_bulk_urb(urb, dev->udev,
			  usb_sndbulkpipe(dev->udev, dev->bulk_out_endpointAddr),
			  buf, writesize, ez_write_bulk_callback, dev);
	urb->transfer_flags |= URB_NO_TRANSFER_DMA_MAP;
	usb_anchor_urb(urb, &dev->submitted);

	/* send the data out the bulk port */
	retval = usb_submit_urb(urb, GFP_KERNEL);
	mutex_unlock(&dev->io_mutex);
	if (retval) {
		dev_err(&dev->interface->dev,
			"%s - failed submitting write urb, error %d\n",
			__func__, retval);
		goto error_unanchor;
	}

	/*
	 * release our reference to this urb, the USB core will eventually free
	 * it entirely
	 */
	usb_free_urb(urb);


	return writesize;

error_unanchor:
	usb_unanchor_urb(urb);
error:
	if (urb) {
		usb_free_coherent(dev->udev, writesize, buf, urb->transfer_dma);
		usb_free_urb(urb);
	}
	up(&dev->limit_sem);

exit:
	return retval;
}


static struct file_operations ez_fops = {
	.owner          = THIS_MODULE,
	.unlocked_ioctl = ez_ioctl,
	.read           = ez_read,
	.write          = ez_write,
	.open           = ez_open,
	.release        = ez_release,
	.flush          = ez_flush,
	.llseek         = noop_llseek,
};

/* 
 * usb class driver info in order to get a minor number from the usb core,
 * and to have the device registered with the driver core
 */
static struct usb_class_driver ez_class = {
	.name       = "usb/ez%d",
	.fops       = &ez_fops,
	//.mode = S_IFCHR | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH,
	.minor_base = USB_EZ_MINOR_BASE,
};

static int ez_probe(struct usb_interface *interface, const struct usb_device_id *id)
{
	struct usb_ez *dev;
	int retval;

	/* allocate memory for our device state and initialize it */
	dev = kzalloc(sizeof(*dev), GFP_KERNEL);
	if (!dev)
		return -ENOMEM;

	kref_init(&dev->kref);
	sema_init(&dev->limit_sem, WRITES_IN_FLIGHT);
	mutex_init(&dev->io_mutex);
	spin_lock_init(&dev->err_lock);
	init_usb_anchor(&dev->submitted);
	init_waitqueue_head(&dev->bulk_in_wait);

	dev->udev = usb_get_dev(interface_to_usbdev(interface));
	dev->interface = usb_get_intf(interface);
	
#if 0
	{
		struct usb_endpoint_descriptor *bulk_in, *bulk_out;
			/* set up the endpoint information */
			/* use only the first bulk-in and bulk-out endpoints */
		retval = usb_find_common_endpoints(interface->cur_altsetting,
				&bulk_in, &bulk_out, NULL, NULL);
		if (retval) {
			dev_err(&interface->dev,
				"Could not find both bulk-in and bulk-out endpoints\n");
			goto error;
		}
		
		dev->bulk_in_size = usb_endpoint_maxp(bulk_in);
		dev->bulk_in_endpointAddr = bulk_in->bEndpointAddress;
		dev->bulk_in_buffer = kmalloc(dev->bulk_in_size, GFP_KERNEL);
		if (!dev->bulk_in_buffer) {
			retval = -ENOMEM;
			goto error;
		}
		dev->bulk_in_urb = usb_alloc_urb(0, GFP_KERNEL);
		if (!dev->bulk_in_urb) {
			retval = -ENOMEM;
			goto error;
		}
		
		dev->bulk_out_endpointAddr = bulk_out->bEndpointAddress;
	}
#endif
	
	{
		unsigned index;
		
		index=(id-ez_table);
		if( index < sizeof(ez_types)/sizeof(*ez_types) )
		{
			dev->type=ez_types[index];
		}
		else
		{
			pr_info("index is %d\n", index);
			dev->type=0;
		}
	}

	/* save our data pointer in this interface device */
	usb_set_intfdata(interface, dev);

	ez_cpus(dev,1);

	/* we can register the device now, as it is ready */
	retval = usb_register_dev(interface, &ez_class);
	if (retval) {
		/* something prevented us from registering this driver */
		dev_err(&interface->dev,
			"Not able to get a minor for this device.\n");
		usb_set_intfdata(interface, NULL);
		goto error;
	}

	/* let the user know what node this device is now attached to */
	dev_info(&interface->dev,
		 "USB ez #%d now attached",
		 interface->minor);
	return 0;

error:
	kref_put(&dev->kref, ez_delete);
	return retval;
}

static void ez_disconnect(struct usb_interface *interface)
{
	struct usb_ez *dev;
	int minor = interface->minor;

	dev = usb_get_intfdata(interface);
	usb_set_intfdata(interface, NULL);

	/* give back our minor */
	usb_deregister_dev(interface, &ez_class);

	/* prevent more I/O from starting */
	mutex_lock(&dev->io_mutex);
	dev->disconnected = 1;
	mutex_unlock(&dev->io_mutex);

	usb_kill_urb(dev->bulk_in_urb);
	usb_kill_anchored_urbs(&dev->submitted);

	/* decrement our usage count */
	kref_put(&dev->kref, ez_delete);

	dev_info(&interface->dev, "USB ez #%d now disconnected", minor);
}

static void ez_draw_down(struct usb_ez *dev)
{
	int time;

	time = usb_wait_anchor_empty_timeout(&dev->submitted, 1000);
	if (!time)
		usb_kill_anchored_urbs(&dev->submitted);
	usb_kill_urb(dev->bulk_in_urb);
}

static int ez_suspend(struct usb_interface *intf, pm_message_t message)
{
	struct usb_ez *dev = usb_get_intfdata(intf);

	if (!dev)
		return 0;
	ez_draw_down(dev);
	return 0;
}

static int ez_resume(struct usb_interface *intf)
{
	return 0;
}

static int ez_pre_reset(struct usb_interface *intf)
{
	struct usb_ez *dev = usb_get_intfdata(intf);

	mutex_lock(&dev->io_mutex);
	ez_draw_down(dev);

	return 0;
}

static int ez_post_reset(struct usb_interface *intf)
{
	struct usb_ez *dev = usb_get_intfdata(intf);

	/* we are sure no URBs are active - no locking needed */
	dev->errors = -EPIPE;
	mutex_unlock(&dev->io_mutex);

	return 0;
}

static struct usb_driver ez_driver = {
	.name                 = "ez",
	.probe                = ez_probe,
	.disconnect           = ez_disconnect,
	.suspend              = ez_suspend,
	.resume               = ez_resume,
	.pre_reset            = ez_pre_reset,
	.post_reset           = ez_post_reset,
	.id_table             = ez_table,
	.supports_autosuspend = 1,
};

module_usb_driver(ez_driver);

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("TLa (thierry.larmoire@free.fr)");
MODULE_DESCRIPTION("usb ez like driver (an, fx and fx2");
MODULE_INFO(intree, "N");
