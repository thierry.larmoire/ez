#if 0
set file [info script]

proc depend { left rights make } {
	set doit 0
	if { ![file exists $left] } {
		#puts stdout "[pwd]/$left <- $rights n"
		incr doit
	} {
		foreach right $rights {
			if { [file mtime $left]<=[file mtime $right] } {
				#puts stdout "$left <- $rights d"
				incr doit
				break
			}
		}
	}
	if { $doit } {
		uplevel $make
	}
}

file mkdir .build
set o .build/[file rootname $file].so

depend $o [list $file] {
	exec gcc -pipe -shared -fPIC -o $o $file -I/usr/src/linux/include
	puts stdout "$o done"
}

source ez.tcl

ez::open
ez::reset 1

set code [binary format c* {
		0x75 0xb1 0x00
		0x00 0x00 0x00 0x00
		0x78 0xaa
		0x02 0x00 0x04
		0xa6 0x00
		0x76 0x22
		0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00
		0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00
		0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00
}]
ez::load 0 $code
ez::reset 0
ez::close

return
#endif

#include "../driver/ez.h"
#include <tcl/tcl.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>


#define MODULE ez
#define MODULE_NAME STRINGIFY(MODULE)
#define TclInit Ez_Init

#define TCL_CMD(func) \
	int proc_ ## func _ANSI_ARGS_((ClientData clientData,\
		Tcl_Interp *interp, int objc, struct Tcl_Obj *CONST objv[]))

#define TCL_PROCS \
	TCL_PROC(open) \
	TCL_PROC(close) \
	TCL_PROC(reset) \
	TCL_PROC(r) \
	TCL_PROC(w) \
/**/

#define TCL_PROC(func) TCL_CMD(func);
TCL_PROCS
#undef TCL_PROC

int fd=-1;

int TclInit(Tcl_Interp * interp)
{
	fd=-1;

#define TCL_PROC(func) Tcl_CreateObjCommand(interp, MODULE_NAME "::" #func, proc_ ## func,0/*clientdata*/,NULL/*deleteProc*/);
TCL_PROCS
#undef TCL_PROC

	return TCL_OK;
}


TCL_CMD(open)
{
	char *device="/dev/" MODULE_NAME "1";

	if(objc>=2)
		device=Tcl_GetString(objv[1]);

	if(fd>=0)
		close(fd);

	fd=open(device,0);

	if(fd>=0)
		return TCL_OK;

	Tcl_ResetResult(interp);
	Tcl_AppendStringsToObj(Tcl_GetObjResult(interp),
		"can\'t open \"", device, "\"", NULL);

	return TCL_ERROR;
}

TCL_CMD(close)
{
	if(fd>=0)
		close(fd);
	fd=-1;

	return TCL_OK;
}

TCL_CMD(reset)
{
	int value;

	if(fd<0)
		return TCL_ERROR;

	if( objc<2
	 || Tcl_GetIntFromObj(interp,objv[1], &value)!=TCL_OK
	)
	{
		Tcl_ResetResult(interp);
		Tcl_AppendStringsToObj(Tcl_GetObjResult(interp),
			Tcl_GetString(objv[0])," value", NULL);

		return TCL_ERROR;
	}

	if(ioctl(fd,EZ_SET_CPU_RESET,&value)<0)
		return TCL_ERROR;

	return TCL_OK;
}

TCL_CMD(r)
{
	int address,value;
	__u32 data;

	struct ez_internal_xfer xfer;
	if(objc<2
	 ||Tcl_GetIntFromObj(interp,objv[1], &address)!=TCL_OK
	)
	{
		Tcl_AppendStringsToObj(Tcl_GetObjResult(interp),
			Tcl_GetString(objv[0])," address", NULL);
		return TCL_ERROR;
	}

	xfer.address=address;
	xfer.data=&data;
	xfer.size=sizeof(data);
	if(ioctl(fd,EZ_INTERNAL_READ,&xfer)!=sizeof(data))
		return TCL_ERROR;

	value=data;
	Tcl_SetIntObj(Tcl_GetObjResult(interp),value);

	return TCL_OK;
}

TCL_CMD(w)
{
	int address,value;
	__u32 data;

	struct ez_internal_xfer xfer;
	if(objc<3
	 ||Tcl_GetIntFromObj(interp,objv[1], &address)!=TCL_OK
	 ||Tcl_GetIntFromObj(interp,objv[2], &value)!=TCL_OK
	)
	{
		Tcl_AppendStringsToObj(Tcl_GetObjResult(interp),
			Tcl_GetString(objv[0])," address data", NULL);
		return TCL_ERROR;
	}

	xfer.address=address;
	xfer.data=&data;
	xfer.size=sizeof(data);
	data=value;
	if(ioctl(fd,EZ_INTERNAL_WRITE,&xfer)!=sizeof(data))
		return TCL_ERROR;

	return TCL_OK;
}

#if 0
TCL_CMD(read)
{
	struct XferBuffer xferBuffer;

	if(fd<0)
	{
		Tcl_ResetResult(interp);
		Tcl_AppendStringsToObj(Tcl_GetObjResult(interp),
			"device is close", NULL);

		return TCL_ERROR;
	}

	if(objc<3
	 ||Tcl_GetIntFromObj(interp,objv[1], &xferBuffer.offset)!=TCL_OK
	 ||Tcl_GetIntFromObj(interp,objv[2], &xferBuffer.len)!=TCL_OK)
	{
		Tcl_ResetResult(interp);
		Tcl_AppendStringsToObj(Tcl_GetObjResult(interp),
			Tcl_GetString(objv[0])," address length", NULL);

		return TCL_ERROR;
	}

	xferBuffer.address = (void*)Tcl_SetByteArrayLength(Tcl_GetObjResult(interp), xferBuffer.len);

	if(ioctl(fd,VIDIOC_MEM_READ,&xferBuffer)!=0)
		return TCL_ERROR;

	return TCL_OK;
}

TCL_CMD(write)
{
	struct XferBuffer xferBuffer;

	if(fd<0)
	{
		Tcl_ResetResult(interp);
		Tcl_AppendStringsToObj(Tcl_GetObjResult(interp),
			"device is close", NULL);

		return TCL_ERROR;
	}

	if(objc<3
	 ||Tcl_GetIntFromObj(interp,objv[1], &xferBuffer.offset)!=TCL_OK)
	{
		Tcl_ResetResult(interp);
		Tcl_AppendStringsToObj(Tcl_GetObjResult(interp),
			Tcl_GetString(objv[0])," address data", NULL);

		return TCL_ERROR;
	}

	xferBuffer.address = (void*)Tcl_GetByteArrayFromObj(objv[2], &xferBuffer.len);

	if(ioctl(fd,VIDIOC_MEM_WRITE,&xferBuffer)!=0)
		return TCL_ERROR;

	return TCL_OK;
}

#endif
