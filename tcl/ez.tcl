

load [file dir [info script]]/.build/ez.so

proc ez::load { address data } {
	binary scan $data i* dwords

	foreach dword $dwords {
		#puts stdout [format 0x%08x $dword]
		ez::w $address $dword
		incr address 4
	}
}

proc ez::loadIhex { file } {
	set buf [binary format a0 ""]

	set fd [::open $file {RDONLY}]
	while { [gets $fd line]!=-1} {
		scan $line {:%2x%4x%2x} count address type
		if { $type==0 } {
			set hex [string range $line 9 [expr 8+2*$count]]
			scan [string range $line [expr 9+$count-1] [expr 9+$count+1]] {%2x} chksum
			set data [binary format H* $hex]
			#puts stdout [format "%d 0x%04x %d %s" $type $address $count $hex]
			set buf [binary format a*@[set address]a* $buf $data]
		}
	}
	::close $fd

	set buf [binary format a*a4 $buf ""]

	puts stdout [format "%s %d" $file [string length $buf]]
	#binary scan $buf H* hex
	#puts stdout $hex
	
	#binary scan $bytes c* bytes
	#foreach byte $bytes {
	#	puts stdout [format 0x%02x [expr $byte&0xff]]
	#}
	load 0 $buf
}

proc ez::dump { from to } {
	set dwords [list]
	for { set i $from } { $i<$to } { incr i 4 } {
		lappend dwords [ez::r [expr 0x0000+$i]]
	}
	set bytes [binary format i* $dwords]
	binary scan $bytes H* hex
	set hex
}

