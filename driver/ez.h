#ifndef _LINUX_USBDEVICE_EZ_H
#define _LINUX_USBDEVICE_EZ_H

#include <linux/ioctl.h>
#include <linux/types.h>

struct ez_internal_xfer
{
	unsigned address;
	void *data;
	unsigned size;
};

#define EZ_IOCTLS \
	/*      (right,name,type) */ \
	EZ_IOCTL(W,SET_CPU_RESET,unsigned int) \
	EZ_IOCTL(R,GET_CPU_RESET,unsigned int) \
	EZ_IOCTL(B,INTERNAL_WRITE,struct ez_internal_xfer) \
	EZ_IOCTL(B,INTERNAL_READ,struct ez_internal_xfer) \
/**/


// make of nr
enum {
	#define EZ_IOCTL(r,name,type) ez_ioctl_nr_ ## name,
	EZ_IOCTLS
	#undef EZ_IOCTL
};

// make of IOCTL argument type
#define EZ_IOCTL(r,name,type) typedef type ez_ioctl_type_ ## name;
EZ_IOCTLS
#undef EZ_IOCTL

#define EZ_IOCTL_N(name) _IOC( _IOC_NONE            , 'U',ez_ioctl_nr_ ## name,0                                     )
#define EZ_IOCTL_R(name) _IOC( _IOC_READ            , 'U',ez_ioctl_nr_ ## name,_IOC_TYPECHECK(ez_ioctl_type_ ## name))
#define EZ_IOCTL_W(name) _IOC( _IOC_WRITE           , 'U',ez_ioctl_nr_ ## name,_IOC_TYPECHECK(ez_ioctl_type_ ## name))
#define EZ_IOCTL_B(name) _IOC( _IOC_READ|_IOC_WRITE , 'U',ez_ioctl_nr_ ## name,_IOC_TYPECHECK(ez_ioctl_type_ ## name))

// make of IOCTL cmd them self
enum {
	#define EZ_IOCTL(r,name,type) EZ_ ## name = EZ_IOCTL_ ## r (name),
	EZ_IOCTLS
	#undef EZ_IOCTL
};


/*
#define EZ_SET_CPU_RESET    _IOW('U', 0, unsigned int)
#define EZ_GET_CPU_RESET    _IOR('U', 1, unsigned int)
#define EZ_INTERNAL_WRITE   _IOW('U', 2, struct ez_internal_xfer)
#define EZ_INTERNAL_READ    _IOR('U', 3, struct ez_internal_xfer)

#define USBDEVFS_CONTROL           _IOWR('U', 0, unsigned int)
#define USBDEVFS_BULK              _IOWR('U', 2, struct usbdevfs_bulktransfer)
#define USBDEVFS_RESETEP           _IOR('U', 3, unsigned int)
#define USBDEVFS_SETINTERFACE      _IOR('U', 4, struct usbdevfs_setinterface)
#define USBDEVFS_SETCONFIGURATION  _IOR('U', 5, unsigned int)
#define USBDEVFS_GETDRIVER         _IOW('U', 8, struct usbdevfs_getdriver)
#define USBDEVFS_SUBMITURB         _IOR('U', 10, struct usbdevfs_urb)
#define USBDEVFS_DISCARDURB        _IO('U', 11)
*/

#endif /* _LINUX_USBDEVICE_EZ_H */
