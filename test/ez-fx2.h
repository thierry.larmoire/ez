#ifndef __ez_fx2_h__
#define __ez_fx2_h__

// from cy7c68013.pdf
// Document #: 38-08012 Rev. *E

xdata at 0xe400 unsigned char WAVEDATA[128];

xdata at 0xe600 unsigned char CPUCS;
xdata at 0xe601 unsigned char IFCONFIG;
xdata at 0xe602 unsigned char PINFLAGSAB;
xdata at 0xe603 unsigned char PINFLAGSCD;
xdata at 0xe604 unsigned char FIFORESET;
xdata at 0xe605 unsigned char BREAKPT;
xdata at 0xe606 unsigned char BPADDRH;
xdata at 0xe607 unsigned char BPADDRL;
xdata at 0xe608 unsigned char UART230;
xdata at 0xe609 unsigned char FIFOPINPOLAR;
xdata at 0xe60a unsigned char REVID;
xdata at 0xe60b unsigned char REVCTL;
xdata at 0xe60c unsigned char GPIFHOLDTIME;

xdata at 0xe610 unsigned char EP1OUTCFG;
xdata at 0xe611 unsigned char EP1INCFG;
xdata at 0xe612 unsigned char EP2CFG;
xdata at 0xe613 unsigned char EP4CFG;
xdata at 0xe614 unsigned char EP6CFG;
xdata at 0xe615 unsigned char EP8CFG;

xdata at 0xe620 unsigned char EP2AUTOINLENH;
xdata at 0xe621 unsigned char EP2AUTOINLENL;
xdata at 0xe622 unsigned char EP4AUTOINLENH;
xdata at 0xe623 unsigned char EP4AUTOINLENL;
xdata at 0xe624 unsigned char EP6AUTOINLENH;
xdata at 0xe625 unsigned char EP6AUTOINLENL;
xdata at 0xe626 unsigned char EP8AUTOINLENH;
xdata at 0xe627 unsigned char EP8AUTOINLENL;

xdata at 0xe630 unsigned char EP2FIFOPFH;
xdata at 0xe631 unsigned char EP2FIFOPFL;
xdata at 0xe632 unsigned char EP4FIFOPFH;
xdata at 0xe633 unsigned char EP4FIFOPFL;
xdata at 0xe634 unsigned char EP6FIFOPFH;
xdata at 0xe635 unsigned char EP6FIFOPFL;
xdata at 0xe636 unsigned char EP8FIFOPFH;
xdata at 0xe637 unsigned char EP8FIFOPFL;

xdata at 0xe640 unsigned char EP2ISOINPKTS;
xdata at 0xe641 unsigned char EP4ISOINPKTS;
xdata at 0xe642 unsigned char EP6ISOINPKTS;
xdata at 0xe643 unsigned char EP8ISOINPKTS;

xdata at 0xe648 unsigned char INPKTEND;
xdata at 0xe649 unsigned char OUTPKTEND;

xdata at 0xe650 unsigned char EP2FIFOIE;
xdata at 0xe651 unsigned char EP2FIFOIRQ;
xdata at 0xe652 unsigned char EP4FIFOIE;
xdata at 0xe653 unsigned char EP4FIFOIRQ;
xdata at 0xe654 unsigned char EP6FIFOIE;
xdata at 0xe655 unsigned char EP6FIFOIRQ;
xdata at 0xe656 unsigned char EP8FIFOIE;
xdata at 0xe657 unsigned char EP8FIFOIRQ;
xdata at 0xe658 unsigned char IBNIE;
xdata at 0xe659 unsigned char IBNIRQ;
xdata at 0xe65a unsigned char NAKIE;
xdata at 0xe65b unsigned char NAKIRQ;
xdata at 0xe65c unsigned char USBIE;
xdata at 0xe65d unsigned char USBIRQ;
xdata at 0xe65e unsigned char EPIE;
xdata at 0xe65f unsigned char EPIRQ;

xdata at 0xe660 unsigned char GPIFIE;
xdata at 0xe660 unsigned char GPIFIRQ;
xdata at 0xe660 unsigned char USBERRIE;
xdata at 0xe660 unsigned char USBERRIRQ;
xdata at 0xe660 unsigned char ERRCNTLIM;
xdata at 0xe660 unsigned char CLRERRCNT;
xdata at 0xe660 unsigned char INT2IVEC;
xdata at 0xe660 unsigned char INT4IVEC;
xdata at 0xe660 unsigned char INTSETUP;

xdata at 0xe670 unsigned char PORTACFG;
xdata at 0xe671 unsigned char PORTCCFG;
xdata at 0xe672 unsigned char PORTECFG;

xdata at 0xe678 unsigned char I2CS;
xdata at 0xe679 unsigned char I2DAT;
xdata at 0xe67a unsigned char I2CTL;
xdata at 0xe67b unsigned char XAUTODAT1;
xdata at 0xe67c unsigned char XAUTODAT2;
xdata at 0xe67d unsigned char UDMACRCH;
xdata at 0xe67e unsigned char UDMACRCL;
xdata at 0xe67f unsigned char UDMACRCQUALIFIER;

xdata at 0xe680 unsigned char USBCS;
xdata at 0xe681 unsigned char SUSPEND;
xdata at 0xe682 unsigned char WAKEUPCS;
xdata at 0xe683 unsigned char TOGCTL;
xdata at 0xe684 unsigned char USBFRAMEH;
xdata at 0xe685 unsigned char USBFRAMEL;
xdata at 0xe686 unsigned char MICROFRAME;
xdata at 0xe687 unsigned char FNADDR;
/*
xdata at 0xe688 unsigned char ;
*/

xdata at 0xe68a unsigned char EP0BCH;
xdata at 0xe68b unsigned char EP0BCL;

xdata at 0xe68d unsigned char EP1OUTBC;

xdata at 0xe68f unsigned char EP1INBC;
xdata at 0xe690 unsigned char EP2BCH;
xdata at 0xe691 unsigned char EP2BCL;

xdata at 0xe694 unsigned char EP4BCH;
xdata at 0xe695 unsigned char EP4BCL;

xdata at 0xe698 unsigned char EP6BCH;
xdata at 0xe699 unsigned char EP6BCL;

xdata at 0xe69c unsigned char EP8BCH;
xdata at 0xe69d unsigned char EP8BCL;

xdata at 0xe6a0 unsigned char EP0CS;
xdata at 0xe6a1 unsigned char EP1OUTCS;
xdata at 0xe6a2 unsigned char EP1INCS;
xdata at 0xe6a3 unsigned char EP2CS;
xdata at 0xe6a4 unsigned char EP4CS;
xdata at 0xe6a5 unsigned char EP6CS;
xdata at 0xe6a6 unsigned char EP8CS;
xdata at 0xe6a7 unsigned char EP2FIFOFLGS;
xdata at 0xe6a8 unsigned char EP4FIFOFLGS;
xdata at 0xe6a9 unsigned char EP6FIFOFLGS;
xdata at 0xe6aa unsigned char EP8FIFOFLGS;
xdata at 0xe6ab unsigned char EP2FIFOBCH;
xdata at 0xe6ac unsigned char EP2FIFOBCL;
xdata at 0xe6ad unsigned char EP4FIFOBCH;
xdata at 0xe6ae unsigned char EP4FIFOBCL;
xdata at 0xe6af unsigned char EP6FIFOBCH;
xdata at 0xe6b0 unsigned char EP6FIFOBCL;
xdata at 0xe6b1 unsigned char EP8FIFOBCH;
xdata at 0xe6b2 unsigned char EP8FIFOBCL;
xdata at 0xe6b3 unsigned char SUDPTRH;
xdata at 0xe6b4 unsigned char SUDPTRL;
xdata at 0xe6b5 unsigned char SUDPTRCTL;
xdata at 0xe6b8 unsigned char SETUPDAT;

xdata at 0xe6c0 unsigned char GPIFWFSELECT;
xdata at 0xe6c1 unsigned char GPIFIDLECS;
xdata at 0xe6c2 unsigned char GPIFIDLECTL;
xdata at 0xe6c3 unsigned char GPIFCTLCFG;
xdata at 0xe6c4 unsigned char GPIFADRH;
xdata at 0xe6c5 unsigned char GPIFADRL;

xdata at 0xe6c6 unsigned char FLOWSTATE;
xdata at 0xe6c7 unsigned char FLOWLOGIC;
xdata at 0xe6c8 unsigned char FLOWEQ0CTL;
xdata at 0xe6c9 unsigned char FLOWEQ1CTL;
xdata at 0xe6ca unsigned char FLOWHOLDOFF;
xdata at 0xe6cb unsigned char FLOWSTB;
xdata at 0xe6cc unsigned char FLOWSTBEDGE;
xdata at 0xe6cd unsigned char FLOWSTBPERIOD;
xdata at 0xe6ce unsigned char GPIFTCB3;
xdata at 0xe6cf unsigned char GPIFTCB2;
xdata at 0xe6d0 unsigned char GPIFTCB1;
xdata at 0xe6d1 unsigned char GPIFTCB0;
xdata at 0xe6d2 unsigned char EP2GPIFFLGSEL;
xdata at 0xe6d3 unsigned char EP2GPIFPFSTOP;
xdata at 0xe6d4 unsigned char EP2GPIFTRIG;
xdata at 0xe6da unsigned char EP4GPIFFLGSEL;
xdata at 0xe6db unsigned char EP4GPIFPFSTOP;
xdata at 0xe6dc unsigned char EP4GPIFTRIG;
xdata at 0xe6e2 unsigned char EP6GPIFFLGSEL;
xdata at 0xe6e3 unsigned char EP6GPIFPFSTOP;
xdata at 0xe6e4 unsigned char EP6GPIFTRIG;
xdata at 0xe6ea unsigned char EP8GPIFFLGSEL;
xdata at 0xe6eb unsigned char EP8GPIFPFSTOP;
xdata at 0xe6ec unsigned char EP8GPIFTRIG;
xdata at 0xe6f0 unsigned char XGPIFSGLDATH;
xdata at 0xe6f1 unsigned char XGPIFSGLDATLX;
xdata at 0xe6f2 unsigned char XGPIFSGLDATLNOX;
xdata at 0xe6f3 unsigned char GPIFREADYCFG;
xdata at 0xe6f4 unsigned char GPIFREADYSTAT;
xdata at 0xe6f5 unsigned char GPIFABORT;
xdata at 0xe6f6 unsigned char EP0BUF;
xdata at 0xe740 unsigned char EP10UTBUF;
xdata at 0xe780 unsigned char EP1INBUF;
xdata at 0xe7c0 unsigned char EP2FIFOBUF;
xdata at 0xf000 unsigned char EP4FIFOBUF;
xdata at 0xf400 unsigned char EP6FIFOBUF;
xdata at 0xf600 unsigned char EP8FIFOBUF;
/*
xdata at 0xf800 unsigned char ;
xdata at 0xfc00 unsigned char ;
xdata at 0xfe00 unsigned char ;
*/







sfr at 0x80 IOA;
sfr at 0x81 SP;
sfr at 0x82 DPL0;
sfr at 0x83 DPH0;
sfr at 0x84 DPL1;
sfr at 0x85 DPH1;
sfr at 0x86 DPS;
sfr at 0x87 PCON;
sfr at 0x88 TCON;
sfr at 0x89 TMOD;
sfr at 0x8a TL0;
sfr at 0x8b TL1;
sfr at 0x8c TH0;
sfr at 0x8d TH1;
sfr at 0x8e CKCON;

sfr at 0x90 IOB;
sfr at 0x91 EXIF;
sfr at 0x92 MPAGE;

sfr at 0x98 SCON0;
sfr at 0x99 SBUF0;
sfr at 0x9a AUTOPTRH1;
sfr at 0x9b AUTOPTRL1;

sfr at 0x9d AUTOPTRH2;
sfr at 0x9e AUTOPTRL2;

sfr at 0xa0 IOC;
sfr at 0xa1 INT2CLR;
sfr at 0xa2 INT4CLR;

sfr at 0xa8 IE;

sfr at 0xaa EP2468STAT;
sfr at 0xab EP24FIFOFLGS;
sfr at 0xac EP68FIFOFLGS;

sfr at 0xaf AUTOPTRSETUP;


sfr at 0xb0 IOD;
sfr at 0xb1 IOE;
sfr at 0xb2 OEA;
sfr at 0xb3 OEB;
sfr at 0xb4 OEC;
sfr at 0xb5 OED;
sfr at 0xb6 OEE;

sfr at 0xb8 IP;

sfr at 0xba EP01STAT;
sfr at 0xbb GPIFTRIG;

sfr at 0xbd GPIFSGLDATH;
sfr at 0xbe GPIFSGLDATLX;
sfr at 0xbf GPIFSGLDATLNOX;

sfr at 0xc0 SCON1;
sfr at 0xc1 SBUF1;

sfr at 0xc8 T2CON;

sfr at 0xca RCAP2L;
sfr at 0xcb RCAP2H;
sfr at 0xcc TL2;
sfr at 0xcd TH2;

sfr at 0xd0 PSW;

sfr at 0xd8 EICON;

sfr at 0xe0 ACC;

sfr at 0xe8 EIE;

sfr at 0xf0 B;

sfr at 0xf8 EIP;

#define DEF_BITT(base,name) \
	sbit at base+0 name ## 0 ; \
	sbit at base+1 name ## 1 ; \
	sbit at base+2 name ## 2 ; \
	sbit at base+3 name ## 3 ; \
	sbit at base+4 name ## 4 ; \
	sbit at base+5 name ## 5 ; \
	sbit at base+6 name ## 6 ; \
	sbit at base+7 name ## 7 ;

#define DEF_BITS(base,name0,name1,name2,name3,name4,name5,name6,name7) \
	sbit at base+0 name0 ; \
	sbit at base+1 name1 ; \
	sbit at base+2 name2 ; \
	sbit at base+3 name3 ; \
	sbit at base+4 name4 ; \
	sbit at base+5 name5 ; \
	sbit at base+6 name6 ; \
	sbit at base+7 name7 ;

DEF_BITT(0x80,IOA_)
DEF_BITS(0x88,  TF1,  TR1,  TF0,  TR0,  IE1,  IT1,  IE0,  IT0)
DEF_BITT(0x90,IOB_)
DEF_BITS(0x98,SM0_0,SM1_0,SM2_0,REN_0,TB8_0,RB8_0, TI_0, RI_0)
DEF_BITT(0xa0,IOC_)
DEF_BITS(0xa8,   EA,  ES1,  ET2,  ES0,  ET1,  EX1,  ET0,  EX0)
DEF_BITT(0xb0,IOD_)
DEF_BITS(0xb8,_b8_7,  PS1,  PT2,  PS0,  PT1,  PX1,  PT0,  PX0)
DEF_BITS(0xc0,SM0_1,SM1_1,SM2_1,REN_1,TB8_1,RB8_1, TI_1, RI_1)
DEF_BITS(0xc8,  TF2, EXF2, RCLK, TCLK,EXEN2,  TR2,  CT2,CPRL2)
DEF_BITS(0xd0,   CY,   AC,   F0,  RS1,  RS0,   OV,   F1,    P)


#endif /* __ez_fx2_h__ */
