#include "mulotDscr.h"

#include "ez-fx2.h"

// EZ USB FX2 control panel Vendor ID 0h, product ID 2131h
// Lakeview Research VID is 0925h.
#define VID 0x0925
#define PID 0x1234
//#define VID 0x0eb1
//#define PID 0x7007
#define DID 0x0000


void alignDscr()
{
	__asm
		.even
	__endasm;
}


const char DeviceDscr[]=
{
	sizeof(DeviceDscr),    // Descriptor length
	DSCR_DEVICE,           // Descriptor type = DEVICE
	0x00,0x02,             // spec version (BCD) is 2.00
	0,0,0,                 // HID class is defined in the interface descriptor
	64,                    // maxPacketSize
	LSB(VID),MSB(VID),
	LSB(PID),MSB(PID),
	LSB(DID),MSB(DID),
	1,                     // Manufacturer string index
	2,                     // Product string index
	0,                     // Serial number string index
	1,                      // Number of configurations
};

const char DeviceQualDscr[]=
{
	sizeof(DeviceQualDscr), // Descriptor Length
	DSCR_DEVQUAL,           // Descriptor Type
	0x00,0x02,              // spec version (BCD) is 2.00
	0,0,0,                  // Device class, sub-class, and sub-sub-class
	0x04,                   // Max Packet Size
	1,                      // Number of configurations
	0,                      // Reserved
};

const char HighSpeedConfigDscr[]=
{
	sizeof(HighSpeedConfigDscr),   // Descriptor length
	DSCR_CONFIG,          // Descriptor type = CONFIG
	LSB(sizeof(HighSpeedConfigDscr)+sizeof(InterfaceDescriptor)+sizeof(HIDDscr)+sizeof(EpInDscr)/*+sizeof(EpOutDscr)*/),
	MSB(sizeof(HighSpeedConfigDscr)+sizeof(InterfaceDescriptor)+sizeof(HIDDscr)+sizeof(EpInDscr)/*+sizeof(EpOutDscr)*/),
	0x01,                // number of interfaces
	0x01,                // value to select this interface
	0x03,                // string index to describe this config
	0xa0,                // b7=1; b6=self-powered; b5=Remote WU
	0,                  // bus power = 80 ma
};

const char InterfaceDescriptor[]=
{
	sizeof(InterfaceDescriptor), // Descriptor length
	DSCR_INTRFC,                 // Descriptor type = INTERFACE
	0,0,                         // Interface 0, Alternate setting 0
	0x01,                        // number of endpoints
	0x03,0x01,0x02,              // class(03)HID, no subclass or protocol
	0x0,                         // string index for this interface
};

const char HIDDscr[]=
{
	sizeof(HIDDscr),  // Descriptor length
	0x21,             // Descriptor type - HID
	0x10,0x01,        // HID Spec version 1.10
	0,                // country code(none)
	0x01,             // number of HID class descriptors
	0x22,             // class descriptor type: REPORT
	LSB(sizeof(ReportDscr)),
	MSB(sizeof(ReportDscr)),
};

const char EpInDscr[]= // I-0, AS-0 first endpoint descriptor (EP1IN)
{
	sizeof(EpInDscr), // Descriptor length
	DSCR_ENDPNT,      // Descriptor type = ENDPOINT
	0x81,             // IN-1
	0x03,             // Type: INTERRUPT
	64,0,             // MaxPacketSize = 64
	0x05,             // polling interval is 2^(5-1) = 16 mSec
};

/*
const char EpOutDscr[]=
{
	sizeof(EpOutDscr), // Descriptor length
	DSCR_ENDPNT,       // Descriptor type = ENDPOINT
	0x01,              // OUT-1
	0x03,              // Type ; INTERRUPT
	64,0,              // MaxPacketSize = 64
	0x05,              // polling interval is 2^(5-1) = 16 mSec
};
*/

const char FullSpeedConfigDscrAlign[1]= { 0 };

const char FullSpeedConfigDscr[]=
{
	sizeof(FullSpeedConfigDscr), // Descriptor length
	DSCR_OTHERSPEED,             // Descriptor type = OTHER SPEED CONFIG
	LSB(sizeof(FullSpeedConfigDscr)+sizeof(FullSpeedIntrfcDscr)+sizeof(FullSpeedHIDDscr)+sizeof(FSEpInDscr)/*+sizeof(FSEpOutDscr)*/),
	MSB(sizeof(FullSpeedConfigDscr)+sizeof(FullSpeedIntrfcDscr)+sizeof(FullSpeedHIDDscr)+sizeof(FSEpInDscr)/*+sizeof(FSEpOutDscr)*/),
	0x01,                        // Number of interfaces
	0x01,                        // Value to select this interface
	0x04,                        // string index to describe this config
	0xa0,                        // b7=1; b6=self-powered; b5=Remote WU
	0,                           // bus power = 80 ma
};

const char FullSpeedIntrfcDscr[]= // Interface Descriptor
{
	sizeof(FullSpeedIntrfcDscr), // Descriptor length
	DSCR_INTRFC,                 // Descriptor type: INTERFACE
	0,0,                         // Interface 0, Alternate setting 0
	0x01,                        // Number of endpoints
	0x03,0x01,0x02,              // Class(03)HID, no subclass or protocol
	0,                           // string index for this interface
};

const char FullSpeedHIDDscr[]=
{
	sizeof(FullSpeedHIDDscr), // Descriptor length
	0x21,                     // Descriptor type - HID
	0x10,0x01,                // HID Spec version 1.10
	0,                        // country code(none)
	0x01,                     // number of HID class descriptors
	0x22,                     // class descriptor type: REPORT
	LSB(sizeof(ReportDscr)),
	MSB(sizeof(ReportDscr)),
};

const char FSEpInDscr[]=
{
	sizeof(FSEpInDscr), // Descriptor length
	DSCR_ENDPNT,        // Descriptor type : ENDPOINT
	0x81,               // IN-1
	ET_INT,             // Type: INTERRUPT
	0x40,0,             // maxPacketSize = 64
	0x32,               // polling interval is 50 msec
};
#if 0
const char FSEpOutDscr[]=
{
	sizeof(FSEpOutDscr), // Descriptor length
	DSCR_ENDPNT,         // Descriptor type = ENDPOINT
	0x01,                // OUT-1
	ET_INT,              // type - INTERRUPT
	0x40,0,              // maxPacketSize = 12
	0x01,                // polling interval is 1 msec
};
#endif

const char ReportDscrAlign[]= { 0 };

#if defined(TYPE_MOUSE)
const char ReportDscr[]=
{
	0x05,0x01, // Usage page, generic desktop
	0x09,0x02, // Usage, mouse
	0xa1,0x01, // Collection, application
	0x09,0x01, // Usage, pointer
	0xa1,0x00, // Collection, physical
	0x05,0x09, // Usage page, button
	0x19,0x01, // Usage minimum, 1
	0x29,0x03, // Usage maximum, 3
	0x15,0x00, // Logical minimum, 0
	0x25,0x01, // Logical maximum, 1
	0x95,0x03, // Report count, 6
	0x75,0x01, // Report size, 1
	0x81,0x02, // Input, data variable absolute
	0x95,0x01, // Report count, 1
	0x75,0x05, // Report size, 2
	0x81,0x01, // Input, constant
	0x05,0x01, // Usage page, generic desktop
	0x09,0x30, // Usage, x
	0x09,0x31, // Usage, y
	0x15,0x81, // Logical minimum, -127
	0x25,0x7f, // Logical maximum, 127
	0x95,0x02, // Report count, 2
	0x75,0x08, // Report size, 8
	0x81,0x06, // Input, data variable relative
	0xC0,      // End Collection (Physical)
	0xC0,      // End Collection (Application)
};
#elif defined(TYPE_KEYBOARD)
const char ReportDscr[]=
{
/*
clavier mac : hub ? 0501 0980 a101 8502 7501 9501 1500 2501 0982 8106 0982 8106 0983 8106 7505 8101 ...
  &         : kbd : 0501 0906 a101 0507 19e0 29e7 1500 2501 7501 9508 8102 9501 7508 8101 9505 7501 ...
*/
	0x05,0x01, // Usage Page (Generic Desktop),
	0x09,0x06, // Usage (Keyboard),
	0xA1,0x01, // Collection (Application),
	0x05,0x07, // Usage Page (Key Codes);
	0x19,0xE0, // Usage Minimum (224),
	0x29,0xE7, // Usage Maximum (231),
	0x15,0x00, // Logical Minimum (0),
	0x25,0x01, // Logical Maximum (1),

	0x75,0x01, // Report Size (1),
	0x95,0x08, // Report Count (8),
	0x81,0x02, // Input (Data, Variable, Absolute)   ; 8 bits for Modifier

	0x95,0x01, // Report Count (1)
	0x75,0x08, // Report Size (8)
	0x81,0x01, // Input (Constant)                   ; 8 bits spare

	0x95,0x05, // Report Count (5)
	0x75,0x01, // Report Size (1)
	0x05,0x08, // Usage Page (Page# for LEDs)
	0x19,0x01, // Usage Minimum (1)
	0x29,0x05, // Usage Maximum (5)
	0x91,0x02, // Output (Data, Variable, Absolute)  ; 5 bits for LED report

	0x95,0x01, // Report Count (1)
	0x75,0x03, // Report Size (3)
	0x91,0x01, // Output (Constant)                  ; 3 bits for padding

	0x95,0x06, // Report Count (6),
	0x75,0x08, // Report Size (8),
	0x15,0x00, // Logical Minimum (0),
	0x25,0x65, // Logical Maximum(101),
	0x05,0x07, // Usage Page (Key Codes),
	0x19,0x00, // Usage Minimum (0),
	0x29,0x65, // Usage Maximum (101),
	0x81,0x00, // Input (Data, Array),               ; 6 bytes for pressed keys
	0xc0       // End Collection
};
#else
#error TYPE_???
#endif

#if 0
const char ReportDscr[]=
{
	0x06, 0xA0, 0xFF, // Usage Page (FFA0h, vendor defined)
	0x09, 0x01,       // Usage (vendor defined)
	0xA1, 0x01,       // Collection (Application)
	0x09, 0x02,       // Usage (vendor defined)
	0xA1, 0x00,       // Collection (Physical)
	0x06, 0xA1, 0xFF, // Usage Page (vendor defined)

// The Input report
	0x09, 0x03, // Usage (vendor defined)
	0x09, 0x04, // Usage (vendor defined)
	0x15, 0x80, // Logical minimum (80h or -128)
	0x25, 0x7F, // Logical maximum (7Fh or 127)
	0x35, 0x00, // Physical minimum (0)
	0x45, 0xFF, // Physical maximum (255)
	0x75, 0x08, // Report size (8 bits)
	0x95, 0x02, // Report count (2 fields)
	0x81, 0x02, // Input (data, variable, absolute)
#if 0
// The Output report
	0x09, 0x05, // Usage (vendor defined)
	0x09, 0x06, // Usage (vendor defined)
	0x15, 0x80, // Logical minimum (80h or -128)
	0x25, 0x7F, // Logical maximum (7Fh or 127)
	0x35, 0x00, // Physical minimum (0)
	0x45, 0xFF, // Physical maximum (255)
	0x75, 0x08, // Report size (8 bits)
	0x95, 0x02, // Report count (2 fields)
	0x91, 0x02, // Output (data, variable, absolute)
#endif

	0xC0,       // End Collection (Physical)
	0xC0,       // End Collection (Application)
};
#endif

const char ReportDscrEnd[]={ 0,0 };


const char StringDscr0[]=
{
	sizeof(StringDscr0),
	DSCR_STRING,
	't',0,
	'b',0,
	'd',0,
};

const char StringDscr1[]=
{
	sizeof(StringDscr1),
	DSCR_STRING,
	'T',0,
	'L',0,
	'a',0,
};

const char StringDscr2[]=
{
	sizeof(StringDscr2),
	DSCR_STRING,
	'M',0,
	'u',0,
	'l',0,
	'o',0,
	't',0,
};

const char StringDscr3[]=
{
	sizeof(StringDscr3),
	DSCR_STRING,
	'H',0,
	'i',0,
	'g',0,
	'h',0,
	' ',0,
	's',0,
	'p',0,
	'e',0,
	'e',0,
	'd',0,
};

const char StringDscr4[]=
{
	sizeof(StringDscr4),
	DSCR_STRING,
	'F',0,
	'u',0,
	'l',0,
	'l',0,
	' ',0,
	's',0,
	'p',0,
	'e',0,
	'e',0,
	'd',0,
};

