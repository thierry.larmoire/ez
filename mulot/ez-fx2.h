#ifndef __ez_fx2_h__
#define __ez_fx2_h__

#define FIELD(len,bit) (len,bit)
#define BIT(bit) (1,bit)

#define _BIT(len,bit) (bit)
#define _LEN(len,bit) (len)
#define _MSK(len,bit) ((~(~0<<len))<<bit)

#define MSK(field) _MSK field
#define CHG(field,value) & ~_MSK field | ((value)<<_BIT field)
#define SET(field) |  _MSK field
#define CLR(field) & ~_MSK field

#define LSB(word) ((unsigned char)(  (unsigned int)(word)     ))
#define MSB(word) ((unsigned char)( ((unsigned int)(word))>>8 ))

// from cy7c68013.pdf
// Document #: 38-08012 Rev. *E

#define DEF_REG(address,name) __xdata volatile unsigned char __at(address) name ;

DEF_REG(0xe400,WAVEDATA[128])

DEF_REG(0xe600,CPUCS)
#define PRTCSTB    BIT(5)
#define CLKSPD     FIELD(2,3)
#define CLKSPD_12 0
#define CLKSPD_24 1
#define CLKSPD_48 2
#define CLKINV     BIT(2)
#define CLKOE      BIT(1)
#define _8051RES   BIT(0)

DEF_REG(0xe601,IFCONFIG)
#define _3048MHZ BIT(6)
DEF_REG(0xe602,PINFLAGSAB)
DEF_REG(0xe603,PINFLAGSCD)
DEF_REG(0xe604,FIFORESET)
DEF_REG(0xe605,BREAKPT)
DEF_REG(0xe606,BPADDRH)
DEF_REG(0xe607,BPADDRL)
DEF_REG(0xe608,UART230)
#define UART230_0 BIT(0)
#define UART230_1 BIT(1)
DEF_REG(0xe609,FIFOPINPOLAR)
DEF_REG(0xe60a,REVID)
DEF_REG(0xe60b,REVCTL)
DEF_REG(0xe60c,GPIFHOLDTIME)

DEF_REG(0xe610,EP1OUTCFG)
DEF_REG(0xe611,EP1INCFG)
DEF_REG(0xe612,EP2CFG)
DEF_REG(0xe613,EP4CFG)
DEF_REG(0xe614,EP6CFG)
DEF_REG(0xe615,EP8CFG)

DEF_REG(0xe620,EP2AUTOINLENH)
DEF_REG(0xe621,EP2AUTOINLENL)
DEF_REG(0xe622,EP4AUTOINLENH)
DEF_REG(0xe623,EP4AUTOINLENL)
DEF_REG(0xe624,EP6AUTOINLENH)
DEF_REG(0xe625,EP6AUTOINLENL)
DEF_REG(0xe626,EP8AUTOINLENH)
DEF_REG(0xe627,EP8AUTOINLENL)

DEF_REG(0xe630,EP2FIFOPFH)
DEF_REG(0xe631,EP2FIFOPFL)
DEF_REG(0xe632,EP4FIFOPFH)
DEF_REG(0xe633,EP4FIFOPFL)
DEF_REG(0xe634,EP6FIFOPFH)
DEF_REG(0xe635,EP6FIFOPFL)
DEF_REG(0xe636,EP8FIFOPFH)
DEF_REG(0xe637,EP8FIFOPFL)

DEF_REG(0xe640,EP2ISOINPKTS)
DEF_REG(0xe641,EP4ISOINPKTS)
DEF_REG(0xe642,EP6ISOINPKTS)
DEF_REG(0xe643,EP8ISOINPKTS)

DEF_REG(0xe648,INPKTEND)
DEF_REG(0xe649,OUTPKTEND)

DEF_REG(0xe650,EP2FIFOIE)
DEF_REG(0xe651,EP2FIFOIRQ)
DEF_REG(0xe652,EP4FIFOIE)
DEF_REG(0xe653,EP4FIFOIRQ)
DEF_REG(0xe654,EP6FIFOIE)
DEF_REG(0xe655,EP6FIFOIRQ)
DEF_REG(0xe656,EP8FIFOIE)
DEF_REG(0xe657,EP8FIFOIRQ)
DEF_REG(0xe658,IBNIE)
DEF_REG(0xe659,IBNIRQ)
DEF_REG(0xe65a,NAKIE)
DEF_REG(0xe65b,NAKIRQ)
DEF_REG(0xe65c,USBIE)
#define EP0ACK     BIT(6)
#define HSGRANT    BIT(5)
#define URES       BIT(4)
#define SUSP       BIT(3)
#define SUTOK      BIT(2)
#define SOF        BIT(1)
#define SUDAV      BIT(0)

DEF_REG(0xe65d,USBIRQ)
DEF_REG(0xe65e,EPIE)
DEF_REG(0xe65f,EPIRQ)

DEF_REG(0xe660,GPIFIE)
DEF_REG(0xe661,GPIFIRQ)
DEF_REG(0xe662,USBERRIE)
DEF_REG(0xe663,USBERRIRQ)
DEF_REG(0xe664,ERRCNTLIM)
DEF_REG(0xe665,CLRERRCNT)
DEF_REG(0xe666,INT2IVEC)
DEF_REG(0xe667,INT4IVEC)
DEF_REG(0xe668,INTSETUP)
#define AV4EN   BIT(0)
#define INT4SRC BIT(1)
#define AV2EN   BIT(3)

DEF_REG(0xe670,PORTACFG)
DEF_REG(0xe671,PORTCCFG)
DEF_REG(0xe672,PORTECFG)

DEF_REG(0xe678,I2CS)
DEF_REG(0xe679,I2DAT)
DEF_REG(0xe67a,I2CTL)
DEF_REG(0xe67b,XAUTODAT1)
DEF_REG(0xe67c,XAUTODAT2)
DEF_REG(0xe67d,UDMACRCH)
DEF_REG(0xe67e,UDMACRCL)
DEF_REG(0xe67f,UDMACRCQUALIFIER)

DEF_REG(0xe680,USBCS)
#define HSM        BIT(7)
#define DISCON     BIT(3)
#define NOSYNSOF   BIT(2)
#define RENUM      BIT(1)
#define SIGRESUME  BIT(0)

DEF_REG(0xe681,SUSPEND)
DEF_REG(0xe682,WAKEUPCS)
DEF_REG(0xe683,TOGCTL)
DEF_REG(0xe684,USBFRAMEH)
DEF_REG(0xe685,USBFRAMEL)
DEF_REG(0xe686,MICROFRAME)
DEF_REG(0xe687,FNADDR)
/*
DEF_REG(0xe688,)
*/

DEF_REG(0xe68a,EP0BCH)
DEF_REG(0xe68b,EP0BCL)

DEF_REG(0xe68d,EP1OUTBC)

DEF_REG(0xe68f,EP1INBC)
DEF_REG(0xe690,EP2BCH)
DEF_REG(0xe691,EP2BCL)

DEF_REG(0xe694,EP4BCH)
DEF_REG(0xe695,EP4BCL)

DEF_REG(0xe698,EP6BCH)
DEF_REG(0xe699,EP6BCL)

DEF_REG(0xe69c,EP8BCH)
DEF_REG(0xe69d,EP8BCL)

DEF_REG(0xe6a0,EP0CS)
#define HSNAK BIT(7)
DEF_REG(0xe6a1,EP1OUTCS)
DEF_REG(0xe6a2,EP1INCS)
#define EPSTALL BIT(0)
#define EPBUSY  BIT(1)
DEF_REG(0xe6a3,EP2CS)
DEF_REG(0xe6a4,EP4CS)
DEF_REG(0xe6a5,EP6CS)
DEF_REG(0xe6a6,EP8CS)
DEF_REG(0xe6a7,EP2FIFOFLGS)
DEF_REG(0xe6a8,EP4FIFOFLGS)
DEF_REG(0xe6a9,EP6FIFOFLGS)
DEF_REG(0xe6aa,EP8FIFOFLGS)
DEF_REG(0xe6ab,EP2FIFOBCH)
DEF_REG(0xe6ac,EP2FIFOBCL)
DEF_REG(0xe6ad,EP4FIFOBCH)
DEF_REG(0xe6ae,EP4FIFOBCL)
DEF_REG(0xe6af,EP6FIFOBCH)
DEF_REG(0xe6b0,EP6FIFOBCL)
DEF_REG(0xe6b1,EP8FIFOBCH)
DEF_REG(0xe6b2,EP8FIFOBCL)
DEF_REG(0xe6b3,SUDPTRH)
DEF_REG(0xe6b4,SUDPTRL)
DEF_REG(0xe6b5,SUDPTRCTL)
#define SDPAUTO BIT(0)
DEF_REG(0xe6b8,SETUPDAT[0])

DEF_REG(0xe6c0,GPIFWFSELECT)
DEF_REG(0xe6c1,GPIFIDLECS)
DEF_REG(0xe6c2,GPIFIDLECTL)
DEF_REG(0xe6c3,GPIFCTLCFG)
DEF_REG(0xe6c4,GPIFADRH)
DEF_REG(0xe6c5,GPIFADRL)

DEF_REG(0xe6c6,FLOWSTATE)
DEF_REG(0xe6c7,FLOWLOGIC)
DEF_REG(0xe6c8,FLOWEQ0CTL)
DEF_REG(0xe6c9,FLOWEQ1CTL)
DEF_REG(0xe6ca,FLOWHOLDOFF)
DEF_REG(0xe6cb,FLOWSTB)
DEF_REG(0xe6cc,FLOWSTBEDGE)
DEF_REG(0xe6cd,FLOWSTBPERIOD)
DEF_REG(0xe6ce,GPIFTCB3)
DEF_REG(0xe6cf,GPIFTCB2)
DEF_REG(0xe6d0,GPIFTCB1)
DEF_REG(0xe6d1,GPIFTCB0)
DEF_REG(0xe6d2,EP2GPIFFLGSEL)
DEF_REG(0xe6d3,EP2GPIFPFSTOP)
DEF_REG(0xe6d4,EP2GPIFTRIG)
DEF_REG(0xe6da,EP4GPIFFLGSEL)
DEF_REG(0xe6db,EP4GPIFPFSTOP)
DEF_REG(0xe6dc,EP4GPIFTRIG)
DEF_REG(0xe6e2,EP6GPIFFLGSEL)
DEF_REG(0xe6e3,EP6GPIFPFSTOP)
DEF_REG(0xe6e4,EP6GPIFTRIG)
DEF_REG(0xe6ea,EP8GPIFFLGSEL)
DEF_REG(0xe6eb,EP8GPIFPFSTOP)
DEF_REG(0xe6ec,EP8GPIFTRIG)
DEF_REG(0xe6f0,XGPIFSGLDATH)
DEF_REG(0xe6f1,XGPIFSGLDATLX)
DEF_REG(0xe6f2,XGPIFSGLDATLNOX)
DEF_REG(0xe6f3,GPIFREADYCFG)
DEF_REG(0xe6f4,GPIFREADYSTAT)
DEF_REG(0xe6f5,GPIFABORT)
DEF_REG(0xe740,EP0BUF[0])
DEF_REG(0xe780,EP1OUTBUF[0])
DEF_REG(0xe7c0,EP1INBUF[0])
DEF_REG(0xf000,EP2FIFOBUF[0])
DEF_REG(0xf400,EP4FIFOBUF[0])
DEF_REG(0xf800,EP6FIFOBUF[0])
DEF_REG(0xfc00,EP8FIFOBUF[0])
/*
DEF_REG(0xf800,)
DEF_REG(0xfc00,)
DEF_REG(0xfe00,)
*/

#define DEF_BITS(base,name7,name6,name5,name4,name3,name2,name1,name0) \
	__sbit __at(base+0) name0 ; \
	__sbit __at(base+1) name1 ; \
	__sbit __at(base+2) name2 ; \
	__sbit __at(base+3) name3 ; \
	__sbit __at(base+4) name4 ; \
	__sbit __at(base+5) name5 ; \
	__sbit __at(base+6) name6 ; \
	__sbit __at(base+7) name7 ;

#define DEF_BITT(base,name) \
	DEF_BITS(base,name ## 0,name ## 1,name ## 2,name ## 3,name ## 4,name ## 5,name ## 6,name ## 7)

#define SFR(address,name) __sfr __at(address) name;
SFR(0x80,IOA)
SFR(0x81,SP)
SFR(0x82,DPL0)
SFR(0x83,DPH0)
SFR(0x84,DPL1)
SFR(0x85,DPH1)
SFR(0x86,DPS)
SFR(0x87,PCON)
#define IDLE   BIT(0)
#define STOP   BIT(1)
#define GF0    BIT(2)
#define GF1    BIT(3)
#define SMOD0  BIT(7)

SFR(0x88,TCON)
SFR(0x89,TMOD)
SFR(0x8a,TL0)
SFR(0x8b,TL1)
SFR(0x8c,TH0)
SFR(0x8d,TH1)
SFR(0x8e,CKCON)
#define MD FIELD(3,0)
#define TM FIELD(3,3)

SFR(0x90,IOB)
SFR(0x91,EXIF)
#define USBNT BIT(4)
SFR(0x92,MPAGE)

SFR(0x98,SCON0)
SFR(0x99,SBUF0)
SFR(0x9a,AUTOPTR1H)
SFR(0x9b,AUTOPTR1L)

SFR(0x9d,AUTOPTR2H)
SFR(0x9e,AUTOPTR2L)

SFR(0xa0,IOC)
SFR(0xa1,INT2CLR)
SFR(0xa2,INT4CLR)

SFR(0xa8,IE)

SFR(0xaa,EP2468STAT)
SFR(0xab,EP24FIFOFLGS)
SFR(0xac,EP68FIFOFLGS)

SFR(0xaf,AUTOPTRSETUP)
#define APTREN BIT(0)

SFR(0xb0,IOD)
SFR(0xb1,IOE)
SFR(0xb2,OEA)
SFR(0xb3,OEB)
SFR(0xb4,OEC)
SFR(0xb5,OED)
SFR(0xb6,OEE)

SFR(0xb8,IP)

SFR(0xba,EP01STAT)
SFR(0xbb,GPIFTRIG)

SFR(0xbd,GPIFSGLDATH)
SFR(0xbe,GPIFSGLDATLX)
SFR(0xbf,GPIFSGLDATLNOX)

SFR(0xc0,SCON1)
SFR(0xc1,SBUF1)

SFR(0xc8,T2CON)

SFR(0xca,RCAP2L)
SFR(0xcb,RCAP2H)
SFR(0xcc,TL2)
SFR(0xcd,TH2)

SFR(0xd0,PSW)

SFR(0xd8,EICON)

SFR(0xe0,ACC)

SFR(0xe8,EIE)

SFR(0xf0,B)

SFR(0xf8,EIP)

DEF_BITS(0xe8, EIE7, EIE6, EIE5,  EX6,  EX5,  EX4, EI2C, EUSB)

DEF_BITS(0xd8,SMOD1,EICON6,ERESI,RESI, INT6,EICON2,EICON1,EICON0)

DEF_BITT(0x80,IOA_)
DEF_BITS(0x88,  TF1,  TR1,  TF0,  TR0,  IE1,  IT1,  IE0,  IT0)
DEF_BITT(0x90,IOB_)
DEF_BITS(0x98,SM0_0,SM1_0,SM2_0,REN_0,TB8_0,RB8_0, TI_0, RI_0)
DEF_BITT(0xa0,IOC_)
DEF_BITS(0xa8,   EA,  ES1,  ET2,  ES0,  ET1,  EX1,  ET0,  EX0)
DEF_BITT(0xb0,IOD_)
DEF_BITS(0xb8,_b8_7,  PS1,  PT2,  PS0,  PT1,  PX1,  PT0,  PX0)
DEF_BITS(0xc0,SM0_1,SM1_1,SM2_1,REN_1,TB8_1,RB8_1, TI_1, RI_1)
DEF_BITS(0xc8,  TF2, EXF2, RCLK, TCLK,EXEN2,  TR2,  CT2,CPRL2)
DEF_BITS(0xd0,   CY,   AC,   F0,  RS1,  RS0,   OV,   F1,    P)

/* USB stuff */

#define ET_CONTROL                       0  // Endpoint type: Control
#define ET_ISO                           1  // Endpoint type: Isochronous
#define ET_BULK                          2  // Endpoint type: Bulk
#define ET_INT                           3  // Endpoint type: Interrupt

#define DSCR_DEVICE                   0x01  // Descriptor type: Device
#define DSCR_CONFIG                   0x02  // Descriptor type: Configuration
#define DSCR_STRING                   0x03  // Descriptor type: String
#define DSCR_INTRFC                   0x04  // Descriptor type: Interface
#define DSCR_ENDPNT                   0x05  // Descriptor type: End Point
#define DSCR_DEVQUAL                  0x06  // Descriptor type: Device Qualifier
#define DSCR_OTHERSPEED               0x07  // Descriptor type: Other Speed Configuration

#define SC_GET_STATUS                 0x00  // Setup command: Get Status
#define SC_CLEAR_FEATURE              0x01  // Setup command: Clear Feature
#define SC_RESERVED                   0x02  // Setup command: Reserved
#define SC_SET_FEATURE                0x03  // Setup command: Set Feature
#define SC_SET_ADDRESS                0x05  // Setup command: Set Address
#define SC_GET_DESCRIPTOR             0x06  // Setup command: Get Descriptor
#define SC_SET_DESCRIPTOR             0x07  // Setup command: Set Descriptor
#define SC_GET_CONFIGURATION          0x08  // Setup command: Get Configuration
#define SC_SET_CONFIGURATION          0x09  // Setup command: Set Configuration
#define SC_GET_INTERFACE              0x0a  // Setup command: Get Interface
#define SC_SET_INTERFACE              0x0b  // Setup command: Set Interface
#define SC_SYNC_FRAME                 0x0c  // Setup command: Sync Frame
#define SC_ANCHOR_LOAD                0xa0  // Setup command: Anchor load

#define GD_DEVICE                     0x01  // Get descriptor: Device
#define GD_CONFIGURATION              0x02  // Get descriptor: Configuration
#define GD_STRING                     0x03  // Get descriptor: String
#define GD_INTERFACE                  0x04  // Get descriptor: Interface
#define GD_ENDPOINT                   0x05  // Get descriptor: Endpoint
#define GD_DEVICE_QUALIFIER           0x06  // Get descriptor: Device Qualifier
#define GD_OTHER_SPEED_CONFIGURATION  0x07  // Get descriptor: Other Configuration
#define GD_INTERFACE_POWER            0x08  // Get descriptor: Interface Power
#define GD_HID                        0x21  // Get descriptor: HID
#define GD_REPORT                     0x22  // Get descriptor: Report

#define GS_DEVICE                     0x80  // Get Status: Device
#define GS_INTERFACE                  0x81  // Get Status: Interface
#define GS_ENDPOINT                   0x82  // Get Status: End Point

#define FT_DEVICE                     0x00  // Feature: Device
#define FT_ENDPOINT                   0x02  // Feature: End Point


#endif /* __ez_fx2_h__ */
