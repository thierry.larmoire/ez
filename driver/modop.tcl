#!/usr/bin/tclsh

set mod ez

cd [file dir [info script]]

proc load {} {
	exec /sbin/insmod $::mod.ko

	set timeout [clock seconds]
	incr timeout 3
	set dev [format "/dev/%s%d" $::mod 0]
	while { $timeout>=[clock seconds] } {
		after 100
		if { [file exists $dev] } {
			exec chown larmoire:larmoire $dev
			break
		}
	}
}

proc unload {} {
	exec rmmod $::mod.ko
}

switch [lindex $argv 0] {
	load {
		load
	}
	unload {
		unload
	}
	reload {
		unload
		load
	}
}