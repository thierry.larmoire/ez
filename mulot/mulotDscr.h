#ifndef __mulotDscr_h__
#define __mulotDscr_h__

extern const char DeviceDscr[];
extern const char ConfigDscr[];
extern const char StringDscr[];
extern const char HIDDscr[];
extern const char ReportDscr[];
extern const char ReportDscrEnd[];
extern const char StringDscr0[];
extern const char StringDscr1[];
extern const char StringDscr2[];
extern const char StringDscr3[];
extern const char StringDscr4[];
extern const char HighSpeedConfigDscr[];
extern const char FullSpeedConfigDscr[];
extern const char DeviceQualDscr[];
extern const char UserDscr[];


#endif /* __mulotDscr_h__ */
