
if 0 {
	for { set j 0 } { $j<4 } { incr j } {
		for { set i 0 } { $i<44 } { incr i } {
			puts stdout [format "	%2d %3d \"\"" [expr $i+1] [expr ($i+$j*44)+1]]
		}
	}
	exit
}

set pinout {
	 1   1 ""
	 2   2 ""
	 3   3 ""
	 4   4 ""
	 5   5 ""
	 6   6 ""
	 7   7 ""
	 8   8 ""
	 9   9 ""
	10  10 ""
	11  11 ""
	12  12 ""
	13  13 ""
	14  14 ""
	15  15 ""
	16  16 ""
	17  17 ""
	18  18 ""
	19  19 ""
	21  21 ""
	22  22 ""
	23  23 ""
	24  24 "PORTD 2"
	25  25 "PORTD 1"
	26  26 "PORTD 0"
	27  27 ""
	28  28 ""
	29  29 ""
	31  31 ""
	32  32 ""
	33  33 ""
	34  34 "VDD ??"
	35  35 "GND"
	36  36 "PORTE 7"
	37  37 "PORTE 6"
	38  38 "PORTE 5"
	39  39 "PORTE 4"
	40  40 "PORTE 3"
	41  41 "PORTE 2"
	42  42 "PORTE 1"
	43  43 "PORTE 0"
	44  44 ""
	
	 1  45 ""
	 2  46 ""
	 3  47 ""
	 4  48 ""
	 5  49 ""
	 6  50 ""
	 7  51 ""
	 8  52 ""
	 9  53 ""
	10  54 ""
	11  55 ""
	12  56 ""
	13  57 ""
	14  58 ""
	15  59 ""
	16  60 ""
	17  61 ""
	18  62 ""
	19  63 ""
	20  64 ""
	21  65 ""
	22  66 ""
	23  67 ""
	24  68 ""
	25  69 ""
	26  70 ""
	27  71 ""
	28  72 ""
	29  73 ""
	30  74 ""
	31  75 ""
	32  76 ""
	33  77 ""
	34  78 ""
	35  79 ""
	36  80 ""
	37  81 ""
	38  82 ""
	39  83 ""
	40  84 ""
	41  85 ""
	42  86 ""
	43  87 ""
	44  88 ""
	
	 1  89 ""
	 2  90 ""
	 3  91 ""
	 4  92 ""
	 5  93 ""
	 6  94 ""
	 7  95 ""
	 8  96 ""
	 9  97 ""
	10  98 ""
	11  99 ""
	12 100 ""
	13 101 ""
	14 102 ""
	15 103 ""
	16 104 ""
	17 105 ""
	18 106 ""
	19 107 ""
	20 108 ""
	21 109 ""
	22 110 ""
	23 111 ""
	24 112 ""
	25 113 ""
	26 114 ""
	27 115 ""
	28 116 ""
	29 117 ""
	30 118 ""
	31 119 ""
	32 120 ""
	33 121 ""
	34 122 ""
	35 123 ""
	36 124 ""
	37 125 ""
	38 126 ""
	39 127 ""
	40 128 ""
	41 129 ""
	42 130 ""
	43 131 ""
	44 132 ""
	
	 1 133 ""
	 2 134 ""
	 3 135 ""
	 4 136 "PORTC 4 5 ?"
	 5 137 ""
	 6 138 ""
	 7 139 ""
	 8 140 ""
	 9 141 ""
	10 142 ""
	11 143 ""
	12 144 ""
	13 145 ""
	14 146 ""
	15 147 ""
	16 148 ""
	17 149 ""
	18 150 ""
	19 151 ""
	20 152 ""
	21 153 ""
	22 154 ""
	23 155 ""
	24 156 ""
	25 157 ""
	26 158 ""
	27 159 ""
	28 160 ""
	29 161 ""
	30 162 ""
	31 163 "PORTC 1"
	32 164 ""
	33 165 ""
	34 166 ""
	35 167 ""
	36 168 ""
	37 169 ""
	38 170 "XTAL"
	39 171 "XTAL"
	40 172 ""
	41 173 ""
	42 174 ""
	43 175 ""
	44 176 ""
}

set w 80
set h 80
set area [binary format a[expr $w*$h] ""]

proc p { x y c } {
	set ::area [binary format a*@[expr $::w*$y+$x]a1 $::area $c]
}

proc rectangle { x y w h {border "+-+| |+-+"} } {
	set border [split $border ""]
	for { set j 0 } { $j<$h } { incr j } {
		for { set i 0 } { $i<$w } { incr i } {
			p [expr $x+$i] [expr $y+$j] [lindex $border [expr (($i!=0)+($i==$w-1))*1+(($j!=0)+($j==$h-1))*3]]
		}
	}
}

proc text { x y dir string } {
	foreach { dx dy } [lindex {
		{+1  0}
		{ 0 +1}
		{-1  0}
		{ 0 -1}
	} $dir ] {}
	if { $dx<0 || $dy<0 } {
		incr x [expr $dx*([string length $string]-1)]
		incr y [expr $dy*([string length $string]-1)]
		set dx [expr -$dx]
		set dy [expr -$dy]
	}
	foreach c [split $string ""] {
		p $x $y $c
		incr x $dx
		incr y $dy
	}
}

rectangle 0 0 $w $h "         "

rectangle 20 20 46 46 "+^+< >+v+"

foreach { i k text } $pinout {
	incr k -1
	incr i -1
	set j [expr $k/44]
	foreach { x y dx dy dir } [lindex {
		{18 21  0 +1 2}
		{21 67 +1  0 1}
		{67 64  0 -1 0}
		{64 18 -1  0 3}
	} $j] {}
	#set text pin$k
	text [expr $x+$dx*$i] [expr $y+$dy*$i] $dir $text
}

for { set i 0 } { $i<$h } { incr i } {
	binary scan $area @[expr $i*$w]a[expr $w] line
	puts stdout $line
}
